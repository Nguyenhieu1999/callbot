/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.bot.dto;

import java.util.Map;

/**
 *
 * @author quangdt
 */
public interface BotDTOFactory {
    CreateCallRequest getInitRequest(String calling, String called, int botId, String botArea, Map<String, String> inputslots);

    CallOutRequest getCalloutRequest(String conversationId, String callbackUrl);
}

package com.its.callbot.bot.dto;


public interface CallOutResponse {

    public Object getMsg();

    public Integer getStatus();
}

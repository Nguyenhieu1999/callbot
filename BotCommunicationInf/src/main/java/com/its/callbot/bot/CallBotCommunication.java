package com.its.callbot.bot;

import com.its.callbot.bot.dto.CallOutRequest;
import com.its.callbot.bot.dto.CallOutResponse;
import com.its.callbot.bot.dto.CallUpdateRequest;
import com.its.callbot.bot.dto.CallUpdateResponse;
import com.its.callbot.bot.dto.CreateCallRequest;
import com.its.callbot.bot.dto.CreateCallResponse;
import java.io.IOException;


public interface CallBotCommunication {

     public <Rq extends CreateCallRequest, Rp extends  CreateCallResponse> Rp createCall(CreateCallRequest request) throws
            IOException;
    
    public <Rq extends CallUpdateRequest, Rp extends  CallUpdateResponse> Rp updateCall(Rq request) throws
            IOException;

    public <Rq extends CallOutRequest, Rp extends  CallOutResponse> Rp callout(Rq request)
            throws IOException;
}

package com.its.callbot.bot.dto;

public interface CreateCallResponse {

    public boolean isSuccess();

    public String getConversationId();
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.repositories.impl;

//import com.its.callbot.commons.entities.CallConversation;
import com.its.callbot.commons.entities.CallConversation;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.Query;
import lombok.extern.log4j.Log4j2;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author quangdt
 */
@Log4j2
@Repository
@Transactional
public class CallConversationRepositoryCustomImpl extends CallRepositoryCustomImpl {
    public List<CallConversation> findInitCall(int retry, int mode,
            int remainder, int limit) {
        if(sqlQueryConversationInit == null){
            log.info("Waiting update SQL Select Init CallConversation");
            return null;
        }
        Query query = entityManager.createNativeQuery(sqlQueryConversationInit)
                .setHint(QueryHints.RESULT_TYPE, ResultType.Map);
        List<Map<String, Object>> lstResult = query.getResultList();
        List<CallConversation> lstCall = new ArrayList<>(lstResult.size());
        for (Map record : lstResult) {
            lstCall.add(convert(record));
        }
        return lstCall;
    }

    private CallConversation convert(Map record) {
        CallConversation call = new CallConversation();
        call.setBotId((Integer)record.get(CallConversation.C_BOT_ID));
        call.setBotRegion((String) record.get(CallConversation.C_BOT_REGION));
        call.setBotUrl((String) record.get(CallConversation.C_BOT_URL));
        call.setConversationId((String) record.get(CallConversation.C_CONVERSATION_ID));
        call.setHotline((String)record.get(CallConversation.C_HOTLINE));
        call.setId((Long) record.get(CallConversation.C_ID));
        call.setIsdn((String) record.get(CallConversation.C_ISDN));
        call.setRetry((Integer) record.get(CallConversation.C_RETRY));
        call.setStatus((Integer) record.get(CallConversation.C_STATUS));
//        call.setInputslots(record);
        return call;
    }

    public static String sqlQueryConversationInit = null;

    public static void setSqlQueryConversationInit(
            String sqlQueryConversationInit) {
        CallConversationRepositoryCustomImpl.sqlQueryConversationInit = sqlQueryConversationInit;
    }

}

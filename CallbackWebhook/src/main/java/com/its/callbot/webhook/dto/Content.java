
package com.its.callbot.webhook.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Content {

    @JsonProperty("pre_action")
    private String preAction;
    @JsonProperty("timestamp")
    private Integer timestamp;
    @JsonProperty("intent_name")
    private String intentName;
    @JsonProperty("action_name")
    private String actionName;
    @JsonProperty("text_user")
    private String textUser;
    @JsonProperty("text_bot")
    private String textBot;
    @JsonProperty("text_tts_bot")
    private String textTtsBot;
    @JsonProperty("status_bot")
    private String statusBot;
    @JsonProperty("timestamp_user")
    private Integer timestampUser;

}

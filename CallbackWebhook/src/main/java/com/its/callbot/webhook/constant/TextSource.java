package com.its.callbot.webhook.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TextSource {

	USER("User:"),
	BOT("Bot:");
	
	
	private String value;
}

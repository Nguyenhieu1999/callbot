/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.webhook.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.its.callbot.callmanage.thresold.CCUCounter;
import com.its.callbot.commons.config.CallManager;
import com.its.callbot.commons.entities.CallLogEntity;
import com.its.callbot.commons.repositories.CallLogRepository;
import com.its.callbot.webhook.constant.TextSource;
import com.its.callbot.webhook.dto.CallResultRequestDTO;
import com.its.callbot.webhook.repository.CallConversationRepository;
import com.its.callbot.webhook.repository.CampaignActionRepository;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 *
 * @author quangdt
 */
@AllArgsConstructor
@Log4j2
public class CallResultService implements Runnable {

    private CallResultRequestDTO dto;

    ObjectMapper mapper;

 
    CallLogRepository callLogRepository;

    CampaignActionRepository campaignActionRepository;


 
    CallConversationRepository callConversationRepository;
    
    private static final Pageable limitedFirstRecord = PageRequest.of(0, 1,
                Sort.by(Sort.Direction.DESC, "id"));
    @Override
    public void run() {
        CallLogEntity callLog = null;
        Page<CallLogEntity> entity = callLogRepository.
                findByBotConversationId(dto.getConversationId(), limitedFirstRecord);
        if (entity != null && !entity.isEmpty()) {
            callLog = entity.getContent().get(0);
        } else {
            CallResultLogger.log(dto);
            return;
        }
        if (dto.getAudioUrl() == null || dto.getAudioUrl().isEmpty()) {
            CCUCounter counter = CallManager.getCampaignCCUCounter(
                    callLog.getCampaignId());
            if (counter != null) {
                counter.decrementAndGet();
            }
            CallManager.ccuCounter.decrementAndGet();
        }
        //update calllog
        updateCallLog(callLog, dto);
        //update call_queue status
        updateCallQueue(dto.getConversationId(), dto.getStatus());
        // update call report
        updateCallReport(callLog, callLog.getCallStatus());
    }

    private void updateCallLog(CallLogEntity callLog, CallResultRequestDTO dto) {
        log.info("update conversation {} with call result {}", callLog.
                getBotConversationId(), dto);

        callLog.setAudioUrl(dto.getAudioUrl());
        try {
            callLog.setBotConversatioDetail(mapper.writeValueAsString(dto.
                    getContent()));
        } catch (JsonProcessingException e1) {
            log.error(e1.getMessage(), e1);
        }
        // get conversation text only
        StringBuilder sb = new StringBuilder();
        dto.getContent().forEach(c -> {
            sb.append("\n").append(TextSource.USER.getValue()).append(c.
                    getTextUser()).append("\n")
                    .append(TextSource.BOT.getValue()).append(c.
                    getTextBot());
        });
        callLog.setBotConversatioText(sb.toString().replaceFirst("\n", ""));
        callLog.setCaller(dto.getCallcenterPhone());
        callLog.setCalled(dto.getCustomerPhone());
        callLog.setCallStatus(dto.getStatus());
        if (dto.getHangupAt() == null || dto.getHangupAt() == 0) {
            dto.setHangupAt(System.currentTimeMillis());
        }
        callLog.setEndTime(new Date(dto.getHangupAt()));

        if (dto.getPickupAt() == null || dto.getPickupAt() == 0) {
            callLog.setStartTime(null);
            callLog.setRingDuration(dto.getHangupAt() - dto.getCallAt());
            callLog.setTalkDuration(0L);
        } else {
            callLog.setStartTime(new Date(dto.getPickupAt()));
            callLog.setRingDuration(dto.getPickupAt() - dto.getCallAt());
            callLog.setTalkDuration(dto.getHangupAt() - dto.getPickupAt());
        }
        callLogRepository.save(callLog);
        log.info("update conversation {} success", callLog.
                getBotConversationId());
    }

    private void updateCallReport(CallLogEntity callLog, Integer status) {
        int numCallSuccess = 0;
        int numCallFail = 0;
        if (status == CallLogEntity.Status.SUCCESS.get()) { // call success
            numCallSuccess = 1 ;
            if (callLog.getRetriedTimes() > 0) {
                numCallFail = -1;
            }
        } else if (callLog.getRetriedTimes() == 0) {
            numCallFail = 1;
        }

        campaignActionRepository.updateReport(callLog.getCampaignActionId(), numCallSuccess, numCallFail);
    }

    private void updateCallQueue(String conversationId, int status) {
        callConversationRepository.updateStatus(conversationId, status);
    }
}

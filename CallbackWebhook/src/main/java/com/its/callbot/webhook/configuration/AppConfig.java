/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.webhook.configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

/**
 *
 * @author quangdt
 */
public class AppConfig {
    @Bean
    @Qualifier(value = "callresult.update.threadpool")
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public ExecutorService getThreadPool(@Value("${threadpool.update.callresult.size}") int poolSize){
        return Executors.newFixedThreadPool(poolSize);
    }
}

package com.its.callbot.webhook.facade;

import com.its.callbot.webhook.dto.CallResultRequestDTO;

public interface CallResultFacade {
    void updateCallResult(CallResultRequestDTO dto);
}

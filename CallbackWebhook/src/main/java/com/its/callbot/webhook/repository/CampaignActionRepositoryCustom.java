/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.webhook.repository;

/**
 *
 * @author quangdt
 */
public interface CampaignActionRepositoryCustom {
    public void updateReport(Long campaignActionId, int numCallSuccess,
            int numCallFail);
}

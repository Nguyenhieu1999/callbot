/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.webhook.services;

import com.its.callbot.webhook.dto.CallResultRequestDTO;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author quangdt
 */
@Log4j2
public class CallResultLogger {
    public static void log(CallResultRequestDTO dto){
        log.info(dto);
    }
}

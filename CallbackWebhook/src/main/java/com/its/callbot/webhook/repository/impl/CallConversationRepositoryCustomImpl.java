/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.webhook.repository.impl;

import com.its.callbot.webhook.repository.CallConversationRepositoryCustom;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Repository
@Transactional
public class CallConversationRepositoryCustomImpl implements CallConversationRepositoryCustom {

    @Autowired
    EntityManager entityManager;
    @Override
    public void updateStatus(String conversationId, int newStatus) {
        log.info("Try update call conversation {} status to {}", conversationId, newStatus);
        try {
            Query query = entityManager.createNativeQuery(SQL_UPDATE_STATUS);
            query.setParameter("converId", conversationId);
            query.setParameter("status", newStatus);

            int rowAffected = query.executeUpdate();
            if(rowAffected > 0){
                log.info("Updated call conversation {} status to {}", conversationId, newStatus);
            }else{
                log.info("Update fail call conversation {} status to {}", conversationId, newStatus);
            }
        } catch (Exception e) {
            log.error("Update error call conversation {} status to {}", conversationId, newStatus, e);
        }
    }
    
    
    static final String SQL_UPDATE_STATUS = "update call_queue set status = :status where conversation_id = :converId";
}

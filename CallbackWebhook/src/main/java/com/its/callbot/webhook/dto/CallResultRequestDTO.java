
package com.its.callbot.webhook.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CallResultRequestDTO {
    @NotNull
    @JsonProperty("conversation_id")
    private String conversationId;
    @JsonProperty("callbot_id")
    private String callbotId;
    @JsonProperty("callcenter_phone")
    private String callcenterPhone;
    @JsonProperty("customer_phone")
    private String customerPhone;
    @JsonProperty("customer_area")
    private String customerArea;
//    @JsonProperty("input_slots")
//    private Object inputSlots;
    @JsonProperty("content")
    private List<Content> content ;
    @JsonProperty("audio_url")
    private String audioUrl;
//    @JsonProperty("created_at")
//    private Long createdAt;
    @JsonProperty("call_at")
    private Long callAt;
    @JsonProperty("pickup_at")
    private Long pickupAt;
    @JsonProperty("hangup_at")
    private Long hangupAt;
    @JsonProperty("status")
    private Integer status;
    @JsonProperty("sip_code")
    private Integer sipCode;
    @JsonProperty("action_end")
    private String actionEnd;
    @JsonProperty("action_path")
    private String actionPath;

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.webhook.repository.impl;

import com.its.callbot.webhook.repository.CampaignActionRepositoryCustom;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Repository
@Transactional
public class CampaignActionRepositoryCustomImpl implements CampaignActionRepositoryCustom {

    @Autowired
    EntityManager entityManager;

    @Override
    public void updateReport(Long campaignActionId, int numCallSuccess,
            int numCallFail) {
        log.info("try update campaign action report campaign action {}, "
                + "number call success {}, number call fail {}",
                campaignActionId, numCallSuccess, numCallFail);
        try {
            Query query = entityManager.createNativeQuery(SQL);
            query.setParameter("numCallSuccess", numCallSuccess);
            query.setParameter("numCallFail", numCallFail);
            query.setParameter("campaignActionId", campaignActionId);

            int rowAffected = query.executeUpdate();
            if (rowAffected > 0) {
                log.info("Updated campaign action report campaign action {}, "
                        + "number call success {}, number call fail {}",
                        campaignActionId, numCallSuccess, numCallFail);
            } else {
                log.info(
                        "Update fail campaign action report campaign action {}, "
                        + "number call success {}, number call fail {}",
                        campaignActionId, numCallSuccess, numCallFail);
            }
        } catch (Exception e) {
            log.error("try update campaign action report campaign action {}, "
                    + "number call success {}, number call fail {}",
                    campaignActionId, numCallSuccess, numCallFail, e);
        }
    }
    String SQL = "update campaign_action set success_call = ifnull(success_call, 0) + :numCallSuccess, "
            + "un_success_call = ifnull(un_success_call, 0) + :numCallFail where id = :campaignActionId";
    
}

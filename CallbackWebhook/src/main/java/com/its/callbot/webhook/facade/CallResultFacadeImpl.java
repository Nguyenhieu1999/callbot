/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.webhook.facade;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.its.callbot.commons.repositories.CallLogRepository;
import com.its.callbot.webhook.dto.CallResultRequestDTO;
import com.its.callbot.webhook.repository.CallConversationRepository;
import com.its.callbot.webhook.repository.CampaignActionRepository;
import com.its.callbot.webhook.services.CallResultService;
import java.util.concurrent.ExecutorService;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Log4j2
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CallResultFacadeImpl implements CallResultFacade {

    private static final Logger logDump = LogManager.
            getLogger("dump.callresult");
    @Autowired
    @Qualifier(value = "callresult.update.threadpool")
    ExecutorService threadpool;

    @Autowired
    ObjectMapper mapper;
    
    @Autowired
    CallLogRepository callLogRepository;
    
    @Autowired
    CampaignActionRepository campaignActionRepository;

    @Autowired
    CallConversationRepository callConversationRepository;

    @PostConstruct
    public void start() {
    }

    @Override
    public void updateCallResult(CallResultRequestDTO dto) {
        log.info("***{}", dto);
        logDump.info(dto);
        CallResultService task = new CallResultService(dto, mapper,
                callLogRepository, campaignActionRepository,
                callConversationRepository);
        threadpool.submit(task);
    }

    @PreDestroy
    public void stop() {
        log.info("Destriy UpdateCallResultServiceImpl");
    }

}

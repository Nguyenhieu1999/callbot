package com.its.callbot.webhook.controller;

import com.its.callbot.webhook.dto.CallResultRequestDTO;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.its.callbot.webhook.facade.CallResultFacade;

@RestController
@RequestMapping(value = "call")
@AllArgsConstructor
@Log4j2
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallResultController {
    @Autowired
    CallResultFacade updateCallResultService;
    

    @PostMapping(value = "result")
    public ResponseEntity<Object> receive(@RequestBody CallResultRequestDTO dto) {
        
        log.info("On update call result {}", dto);
        if(dto == null){
            log.info("On update call result. Missing request body");
            return new ResponseEntity<>("missing request body", HttpStatus.BAD_REQUEST);
        }
        log.info("On update call result {}", dto.getConversationId());
        updateCallResultService.updateCallResult(dto);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

}

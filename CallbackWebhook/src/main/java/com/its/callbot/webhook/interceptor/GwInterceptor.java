package com.its.callbot.webhook.interceptor;

import java.util.Date;
import javax.annotation.PreDestroy;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;


@Log4j2
@Component
public class GwInterceptor implements HandlerInterceptor {

    public static final String requestTime = "requestTime";
    

    @Autowired
    public GwInterceptor() {
    }

    @PreDestroy
    public void onDestroy() {
//        if (limiters != null) {
//            for (Map.Entry<String, GwRateLimiter> entry : limiters.entrySet()) {
//                entry.getValue().release();
//            }
//        }
    }

    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler)
            throws Exception {
        long startTime = System.currentTimeMillis();
        request.setAttribute(requestTime, startTime);
//        String sessionId = request.getSession().getId();
        
//        String jwt = request.getHeader("x-access-token");
//        if (Strings.isBlank(jwt)) {
//            log.info("{} Missing token", sessionId);
//            throw new ProcessErrorException(HttpStatus.UNAUTHORIZED,
//                    NON_AUTHEN_INFO,
//                            messageUtils.getMessage(MessageUtils.ERR_HEADER_001));
//        }
//        if (!jwtTokenFactory.validateJwt(jwt)) {
//            log.info("{} Token invalid", sessionId);
//            throw new ProcessErrorException(HttpStatus.UNAUTHORIZED,
//                    AUTHEN_FAIL,messageUtils.getMessage(MessageUtils.ERR_UNAUTHORIZED));
//        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request,
            HttpServletResponse response, //
            Object handler, ModelAndView modelAndView) throws Exception {

//        Log logContent = null;
//        try {
            log.info("end for request = {} , response = {} in {}ms",
                    request.getSession().getId(), response.getStatus(),
                    (System.currentTimeMillis() - (Long) request.getAttribute(
                    requestTime)));
//            String url = request.getRequestURL().toString();
//            logContent = new Log();
//            logContent.setCode(response.getStatus());
//            logContent.setRequestId(request.getSession().getId());
//            logContent.setUrl(url);
//            logContent.setContent(builder.buildRequestBody(request).toString());
//
//            log.info("Save content {}", logContent);
//            bizLogFacade.saveLog(logContent);
//        } catch (Exception e) {
//            log.error("[postHandle] saveLog {}", logContent, e);
//        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
            HttpServletResponse response, //
            Object handler, Exception ex) throws Exception {
        try{
            log.info("Completed session {} at {}", request.getSession().getId(),
                new Date());
        }catch(Exception e){
            log.error(e.getMessage(), e);
        }
    }

}

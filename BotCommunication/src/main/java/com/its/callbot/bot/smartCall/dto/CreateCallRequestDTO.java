
package com.its.callbot.bot.smartCall.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.its.callbot.bot.dto.CreateCallRequest;

import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CreateCallRequestDTO implements CreateCallRequest{

	@JsonProperty("callbot_id")
	private String callbotId;
	@JsonProperty("callcenter_phone")
	private String callcenterPhone;
	@JsonProperty("customer_phone")
	private String customerPhone;
    @JsonProperty("customer_area")
    private String area;
	@JsonProperty("input_slots")
	Map<String, String> inputSlots = new HashMap<>();
}

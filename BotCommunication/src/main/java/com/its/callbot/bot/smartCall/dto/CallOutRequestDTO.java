package com.its.callbot.bot.smartCall.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.its.callbot.bot.dto.CallOutRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CallOutRequestDTO implements CallOutRequest {

	@JsonProperty("conversation_id")
	private String conversationId;
        @JsonProperty("callback_url")
        private String callbackUrl;
}

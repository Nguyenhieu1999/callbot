package com.its.callbot.bot.smartCall.dto;

import com.its.callbot.bot.dto.CallOutResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter()
@Setter
@Builder
@AllArgsConstructor
@ToString(callSuper = true)
public class CallOutResponseDTO extends BaseResponse implements CallOutResponse{
}

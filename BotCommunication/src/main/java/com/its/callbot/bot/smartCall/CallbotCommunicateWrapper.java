/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.bot.smartCall;

import lombok.Getter;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 *
 * @author quangdt
 */
@Getter
public class CallbotCommunicateWrapper extends AbstractCommunication{
    private CallBotCommunicate bot;
    private final String baseUrl;
    private final int connectionTimeout;
    private final int readTimeout;
    private final int poolSize;
    private final String token;
    private boolean pause = false;

    public CallbotCommunicateWrapper(String baseUrl, int connectionTimeout,
            int readTimeout, int poolSize, String token) {
        this.baseUrl = baseUrl;
        this.connectionTimeout = connectionTimeout;
        this.readTimeout = readTimeout;
        this.poolSize = poolSize;
        this.token = token;
        this.buildSetting();
    }

    
    
    private void buildSetting() {
        OkHttpClient httpClient = this.buildCommunication(connectionTimeout, readTimeout,
                        poolSize, token);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient)
                .build();

        this.bot = retrofit.create(CallBotCommunicate.class);
    }
    
    public synchronized void setPause(boolean isPause){
        pause = isPause;
    }
    
    public boolean isPause(){
        return pause;
    }
}

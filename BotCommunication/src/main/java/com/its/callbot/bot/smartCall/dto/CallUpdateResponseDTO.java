/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.bot.smartCall.dto;

import com.its.callbot.bot.dto.CallUpdateResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author quangdt
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
//@NoArgsConstructor
@ToString(callSuper = true)
public class CallUpdateResponseDTO extends BaseResponse implements CallUpdateResponse{

}

package com.its.callbot.bot.smartCall.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.its.callbot.bot.dto.CreateCallResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
public class CreateCallResponseDTO extends BaseResponse implements CreateCallResponse{

	@JsonProperty("conversation_id")
	private String conversationId;
        
        @JsonIgnore
        private String baseUrl;

}

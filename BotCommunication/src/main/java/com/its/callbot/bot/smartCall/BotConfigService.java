/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.bot.smartCall;

import com.dtsoft.exception.ConfigException;
import com.its.callbot.commons.entities.Config;
import com.its.callbot.commons.repositories.ConfigRepository;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author quangdt
 */
@Component
@Getter
@ToString
@Log4j2
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class BotConfigService {

    int connectionTimeout;
    int readTimeout;
    String[] enpoints;
    String token;
    int poolSize;

    @Autowired
    ConfigRepository repo;

    @PostConstruct
    public void init() throws ConfigException {
//        Config botConfig = Config.builder().key("bot").build();
//        Example<Config> example = Example.of(botConfig, ExampleMatcher.
//                matching()
//                .withIgnoreCase()
//                .withMatcher("param_name", startsWith()));
        List<Config> lstBotConfig = repo.findAllByKeyStartWith("bot%");
        if (lstBotConfig == null || lstBotConfig.isEmpty()) {
            throw new ConfigException("No BOT config");  
        }
        boolean hasError = false;
        for(Config config : lstBotConfig){
            log.info("GOT \"{}\"=\"{}\"", config.getKey(), config.getValue());
            try {
                switch (config.getKey()) {
                case BOT_CONNECTION_TIMEOUT:
                    this.connectionTimeout = config.getIntValue();
                    break;
                case BOT_READ_TIMEOUT:
                    this.readTimeout = config.getIntValue();
                    break;
                case BOT_ENPOINTS:
                    this.enpoints = config.getValue().split(";");
                    break;
                case BOT_TOKEN:
                    this.token = config.getValue();
                    break;
                case BOT_POOL_SIZE:
                    this.poolSize = config.getIntValue();
                    break;
                default:
                    log.info("Ignore config {}={}", config.getKey(), config.
                            getValue());
                }
            } catch (Exception e) {
                log.error("Config invalid: {}", config.getKey(), e);
                hasError = true;
            }
        }
        if(hasError){
            throw new ConfigException("Invalid BOT config");
        }
    }
    public static final String BOT_CONNECTION_TIMEOUT = "bot.connection.timeout";
    public static final String BOT_READ_TIMEOUT = "bot.read.timeout";
    public static final String BOT_ENPOINTS = "bot.enpoints";
    public static final String BOT_TOKEN = "bot.token";
    public static final String BOT_POOL_SIZE = "bot.poolsize";
}

package com.its.callbot.bot.smartCall;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import okhttp3.ConnectionPool;
import okhttp3.Dispatcher;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;

public class AbstractCommunication {

    
    protected OkHttpClient buildCommunication(int connectionTimeout, 
            int readTimeout, int poolSize, String token) {
        Dispatcher dispatcher = new Dispatcher(Executors.newFixedThreadPool(8));
//        dispatcher.setMaxRequests(maxRequest);
//        dispatcher.setMaxRequestsPerHost(maxRequestPerHost);

        HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
        logger.level(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .dispatcher(dispatcher)
                .addInterceptor(logger)
                .addInterceptor(getBearerAuthorizationInterceptor(token))
                .connectTimeout(connectionTimeout, TimeUnit.SECONDS)
                .readTimeout(readTimeout, TimeUnit.SECONDS)
                .connectionPool(new ConnectionPool(poolSize, 60, TimeUnit.SECONDS)).
                build();

        return okHttpClient;
    }

    private Interceptor getBearerAuthorizationInterceptor(String token) {
        return (Interceptor.Chain chain) -> {
            Request newRequest = chain.request().newBuilder()
                    .addHeader("Authorization", "Bearer " + token)
                    .build();
            return chain.proceed(newRequest);
        };
    }
}

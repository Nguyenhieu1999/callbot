package com.its.callbot.bot.smartCall;


import com.its.callbot.bot.smartCall.dto.CallOutRequestDTO;
import com.its.callbot.bot.smartCall.dto.CallOutResponseDTO;
import com.its.callbot.bot.smartCall.dto.CallUpdateRequestDTO;
import com.its.callbot.bot.smartCall.dto.CallUpdateResponseDTO;
import com.its.callbot.bot.smartCall.dto.CreateCallRequestDTO;
import com.its.callbot.bot.smartCall.dto.CreateCallResponseDTO;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;


public interface CallBotCommunicate {

    @POST(value = "api/call/create")
    Call<CreateCallResponseDTO> createCall(@Body CreateCallRequestDTO request);
    
    @POST(value = "api/call/update")
    Call<CallUpdateResponseDTO> updateCall(@Body CallUpdateRequestDTO request);
    
    
    @POST(value = "api/call/callout_safe")
    Call<CallOutResponseDTO> callout(@Body CallOutRequestDTO request);
    
    @GET(value = "health")
    Call<ResponseBody> heathcheck();
}

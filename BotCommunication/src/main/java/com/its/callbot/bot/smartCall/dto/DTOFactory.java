/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.bot.smartCall.dto;

import com.its.callbot.bot.dto.BotDTOFactory;
import com.its.callbot.bot.dto.CallOutRequest;
import com.its.callbot.bot.dto.CreateCallRequest;
import java.util.Map;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 *
 * @author quangdt
 */
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class DTOFactory implements BotDTOFactory{

    @Override
    public CreateCallRequest getInitRequest(String calling, String called,
            int botId, String botArea, Map<String, String> inputSlots) {
            return new CreateCallRequestDTO(String.valueOf(botId), calling,
                called, botArea, inputSlots);
    }

    @Override
    public CallOutRequest getCalloutRequest(String conversationId,
            String callbackUrl) {
            return new CallOutRequestDTO(conversationId, callbackUrl);
    }
    
}

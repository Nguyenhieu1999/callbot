package com.its.callbot.bot.smartCall;

import com.its.callbot.bot.dto.CallOutRequest;
import com.its.callbot.bot.dto.CallUpdateRequest;
import com.its.callbot.bot.dto.CreateCallRequest;
import com.its.callbot.bot.smartCall.dto.CallOutRequestDTO;
import com.its.callbot.bot.smartCall.dto.CallOutResponseDTO;
import com.its.callbot.bot.smartCall.dto.CallUpdateRequestDTO;
import com.its.callbot.bot.smartCall.dto.CallUpdateResponseDTO;
import com.its.callbot.bot.smartCall.dto.CreateCallRequestDTO;
import com.its.callbot.bot.smartCall.dto.CreateCallResponseDTO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import retrofit2.Call;
import retrofit2.Response;

@Log4j2
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CallBotCommunication
        implements com.its.callbot.bot.CallBotCommunication {

    private List<CallbotCommunicateWrapper> botCommunicate = new ArrayList<>();
    static final AtomicInteger botIndex = new AtomicInteger(0);

    @Autowired
    BotConfigService configService;

    @PostConstruct
    public void intConnection() {

        String[] baseUrls = configService.getEnpoints();
        for (String baseUrl : baseUrls) {
            buildWrapper(baseUrl);
        }
    }

    private CallbotCommunicateWrapper buildWrapper(String baseUrl) {
        CallbotCommunicateWrapper wrapper = new CallbotCommunicateWrapper(
                baseUrl,
                configService.getConnectionTimeout(), configService.
                getReadTimeout(),
                configService.getPoolSize(), configService.getToken());
        botCommunicate.add(wrapper);
        return wrapper;
    }

    @Override
    public CreateCallResponseDTO createCall(CreateCallRequest requestCall)
            throws
            IOException {
        if (requestCall instanceof CreateCallRequestDTO) {
            CreateCallRequestDTO request = (CreateCallRequestDTO) requestCall;
            log.info("Update call {}", request);
            long startTime = System.currentTimeMillis();
            try {
                CallbotCommunicateWrapper bot = getActiveBot();
                if (bot == null) {
                    log.error("$NO_ACTIVE_BOT$ No active bot");
                    return null;
                }
                log.info("Create call {} on {} ", request, bot.getBaseUrl());
                Call<CreateCallResponseDTO> data
                        = bot.getBot().createCall(request);
                Response<CreateCallResponseDTO> response = data.execute();
                if (response.isSuccessful()) {
                    CreateCallResponseDTO obj = response.body();
                    log.info("Request= {}\n Response = {}", request, obj);
                    if (obj != null) {
                        obj.setBaseUrl(bot.getBaseUrl());
                    }
                    return obj;
                } else {
                    log.error("Create call fail {}", request, response.
                            toString());
                    return null;
                }
            } finally {
                log.
                        info("Create call to {} in {}ms", request.
                                getCustomerPhone(),
                                (System.currentTimeMillis() - startTime));
            }

        } else {
            throw new MethodArgumentTypeMismatchException(requestCall,
                    CreateCallRequestDTO.class, null, null, null);
        }
    }

    @Override
    public CallUpdateResponseDTO updateCall(CallUpdateRequest requestUpdate)
            throws
            IOException {
        if (requestUpdate instanceof CallUpdateRequestDTO) {
            CallUpdateRequestDTO request = (CallUpdateRequestDTO) requestUpdate;
            log.info("Update call {}", request);
            long startTime = System.currentTimeMillis();
            try {
                CallbotCommunicateWrapper bot = getActiveBot();
                if (null == bot) {
                    log.error("$NO_ACTIVE_BOT$ No active bot");
                    return null;
                }
                log.info("Update call {} on {}", request, bot.getBaseUrl());
                Call<CallUpdateResponseDTO> data
                        = bot.getBot().updateCall(request);
                Response<CallUpdateResponseDTO> response = data.execute();
                if (response.isSuccessful()) {
                    CallUpdateResponseDTO obj = response.body();
                    log.info("Request= {}\n Response = {}", request, obj);
                    return obj;
                } else {
                    log.
                            error("Update call {} fail {}", request, response.
                                    toString());
                    return null;
                }
            } finally {
                log.info("Update call {} in {}ms", request.getConversationId(),
                        (System.currentTimeMillis() - startTime));
            }
        } else {
            throw new MethodArgumentTypeMismatchException(requestUpdate,
                    CallUpdateRequestDTO.class, null, null, null);
        }
    }

    @Override
    public CallOutResponseDTO callout(CallOutRequest requestCallout)
            throws IOException {
        if (requestCallout instanceof CallOutRequestDTO) {
            CallOutRequestDTO request = (CallOutRequestDTO) requestCallout;
            log.info("Create call {}", request);
            long startTime = System.currentTimeMillis();
            try {
                CallbotCommunicateWrapper bot = getActiveBot();
                if (null == bot) {
                    log.error("$NO_ACTIVE_BOT$ No active bot");
                    return null;
                }
                log.info("Create call {} on {}", request, bot.getBaseUrl());
                Call<CallOutResponseDTO> data
                        = bot.getBot().callout(request);
                Response<CallOutResponseDTO> response = data.execute();
                if (response.isSuccessful()) {
                    CallOutResponseDTO obj = response.body();
                    log.info("Request= {}\n Response = {}", request, obj);
                    return obj;
                } else {
                    log.error("Create call fail {}", request, response.
                            toString());
                    return null;
                }
            } finally {
                log.info("Create call {} in {}ms", request.getConversationId(),
                        (System.currentTimeMillis() - startTime));
            }
        } else {
            throw new MethodArgumentTypeMismatchException(requestCallout,
                    CallOutRequestDTO.class, null, null, null);
        }
    }

    public void healthCheck() {
        botCommunicate.forEach((bot) -> {
            healthCheck(bot);
        });
        botCommunicate.forEach((bot) -> {
            log.info("{} {}", bot.getBaseUrl(),
                    bot.isPause() ? " Pause" : " Alive");
        });

    }

    @Async
    private void healthCheck(CallbotCommunicateWrapper bot) {
        long startTime = System.currentTimeMillis();
        try {
            if (null == bot) {
                return;
            }
            log.info("Healthcheck {}", bot.getBaseUrl());
            Call<ResponseBody> data
                    = bot.getBot().heathcheck();
            Response<ResponseBody> response;
            try {
                response = data.execute();
                if (response.isSuccessful()) {
                    log.info("Bot {} is alive", bot.getBaseUrl());
                    bot.setPause(false);
                } else {
                    log.warn("Bot {} is suspended. Pause call to this", bot.
                            getBaseUrl());
                    bot.setPause(true);
                }
            } catch (IOException ex) {
                log.
                        error("Healthcheck {}. Pause call to this", bot.
                                getBaseUrl(), ex);
                bot.setPause(true);
            }

        } finally {
            log.info("Healthcheck {} in {}ms", bot.getBaseUrl(),
                    (System.currentTimeMillis() - startTime));
        }
    }

    private CallbotCommunicateWrapper getActiveBot() {
        synchronized (botIndex) {
            int i = botIndex.get();
            do {
                i++;
                if (i >= botCommunicate.size()) {
                    i = 0;
                }
                CallbotCommunicateWrapper bot = botCommunicate.get(i);
                log.info("{} {}", bot.getBaseUrl(),
                        bot.isPause() ? " Pause" : " Alive");
                if (!bot.isPause()) {
                    botIndex.set(i);
                    return bot;
                }

            } while (i != botIndex.get());
            log.info("======== return null");
            return null;
        }
    }

}

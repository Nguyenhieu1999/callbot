/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.bot.smartCall.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author quangdt
 */
@Getter
@Setter
@ToString
public abstract class BaseResponse {

    protected String msg;
    protected Integer status;
    
    public boolean isSuccess(){
        return 0 == status;
    }
    
}

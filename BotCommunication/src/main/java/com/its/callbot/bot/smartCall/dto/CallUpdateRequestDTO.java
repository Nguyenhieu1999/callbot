/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.bot.smartCall.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.its.callbot.bot.dto.CallUpdateRequest;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author quangdt
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CallUpdateRequestDTO implements CallUpdateRequest{

    @JsonProperty("conversation_id")
    private String conversationId;
    @JsonProperty("input_slots")
    private Map<String, String> inputSlots;
}


import com.its.callbot.bot.smartCall.dto.CreateCallRequestDTO;
import dtsoft.utils.JSonUtil;
import java.util.HashMap;
import java.util.Map;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author quangdt
 */
public class Json {
    public static void main(String[] args) {
        CreateCallRequestDTO request = new CreateCallRequestDTO();
        request.setCallbotId("39");
        request.setCallcenterPhone("0366568956");
        request.setCustomerPhone("0982699437");
        request.setArea("NORTH");
        Map<String, String> inp = new HashMap<>();
        inp.put("Name", "Hieu");
        inp.put("Full", "Nguyen");
        inp.put("Code", "868412");
        request.setInputSlots(inp);
        System.out.println(JSonUtil.toJson(request));
    }
}

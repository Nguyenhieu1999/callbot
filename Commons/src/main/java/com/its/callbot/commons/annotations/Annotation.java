package com.its.callbot.commons.annotations;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.*;

public interface Annotation {

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.FIELD})
    @interface ResponseTime {
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.FIELD})
    @interface ShowAsProperty {
        boolean enabled() default true;
    }


    @ReportAsSingleViolation
    @Constraint(
            validatedBy = {MobilePhoneValidator.class}
    )
    @Documented
    @Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    @interface MobilePhone {
        String message() default "Invalid phone number";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }
    
    
    
    @ReportAsSingleViolation
    @Constraint(
            validatedBy = {MobilePhoneValidator.class}
    )
    @Documented
    @Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    @interface SubPhoneNumber {
        String message() default "Invalid subscriber phone number";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }
    
    @ReportAsSingleViolation
    @Constraint(
            validatedBy = {MobilePhoneValidator.class}
    )
    @Documented
    @Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    @interface Location {
        String message() default "Invalid location";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }

    @Target({ElementType.METHOD, ElementType.FIELD})
    @Retention(RetentionPolicy.RUNTIME)
    @Constraint(
            validatedBy = {EmailValidator.class}
    )
    @Documented
    @interface Email {
        String message() default "not a well-formed email address.";

        int size() default 100;

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }


    @Target({ElementType.METHOD, ElementType.FIELD})
    @Retention(RetentionPolicy.RUNTIME)
    @Constraint(
            validatedBy = {DateTimeValidator.class}
    )
    @Documented
    @interface DateTime {
        String message() default "Invalid date";

        String dateFormat() default "yyyy-MM-dd";

        boolean past() default false;

        boolean future() default false;

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }

    @Target({ElementType.METHOD, ElementType.FIELD})
    @Retention(RetentionPolicy.RUNTIME)
    @Constraint(
            validatedBy = {EnumValidator.class}
    )
    @Documented
    @interface Enumerated {
        String message() default "Invalid enum value";

        boolean allowBlank() default true;

        boolean ignoreCaseSensitive() default true;

        Class<? extends Enum<?>> enumClazz();

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }


    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.FIELD})
    @interface ResponseDateTime {
        String format() default "yyyy-MM-dd'T'HH:mm:ssZ";
    }


}

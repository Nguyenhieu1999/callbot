package com.its.callbot.commons.annotations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.apache.commons.validator.routines.RegexValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;


public class MobilePhoneValidator implements ConstraintValidator<Annotation.MobilePhone, String> {

    @Value(value = "${mobile.pattern}")
    private String mobilePattern;

    public MobilePhoneValidator() {
    }

    @Override
    public void initialize(Annotation.MobilePhone constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (StringUtils.isEmpty(value)) {
            return true;
        } else {
            String template = context.getDefaultConstraintMessageTemplate();
            boolean isValid = true;
            RegexValidator regexValidator = new RegexValidator(mobilePattern);
            if (value.length() > 15) {
                isValid = false;
                template = "ERR_0803";
            } else if (!regexValidator.isValid(value)) {
                isValid = false;
                template = "ERR_0811";
            }

            if (!isValid) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(template).addConstraintViolation();
            }

            return isValid;
        }
    }
}
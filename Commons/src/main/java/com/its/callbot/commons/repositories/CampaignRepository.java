/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.repositories;

import com.its.callbot.commons.entities.Campaign;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 *
 * @author quangdt
 */
@Repository
@Transactional(propagation=Propagation.REQUIRED, readOnly=true, noRollbackFor=Exception.class)
public interface CampaignRepository extends BaseRepository<Campaign, Integer> {
	
    public List<Campaign> findUpdated(@Param("updatedAt") Date lastUpdate);
    
    public List<Campaign> findByStatus(@Param("status") Integer status);
    
//    @Query("select new com.its.callbot.commons.entities.Campaign(c.id) from Campaign c where c.status = 1")

}

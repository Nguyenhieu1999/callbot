/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author quangdt
 */
@Entity
@Table(name = "blacklist")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Blacklist.findAll", query = "SELECT b FROM Blacklist b where b.enable = TRUE")
    ,
        @NamedQuery(name = "Blacklist.findByIsdn",
            query = "SELECT b FROM Blacklist b WHERE b.isdn = :isdn and b.enable = TRUE")})
@Getter
@Setter
@ToString
public class Blacklist extends SpecialCustomer implements Serializable {

}

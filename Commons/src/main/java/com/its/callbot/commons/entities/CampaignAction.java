/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.entities;

import com.its.callbot.commons.entities.converter.ListConverter;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author quangdt
 */
@Entity
@Table(name = "campaign_action")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CampaignAction.findAll",
            query = "SELECT c FROM CampaignAction c")
    ,
        @NamedQuery(name = "CampaignAction.findById",
            query = "SELECT c FROM CampaignAction c WHERE c.id = :id")
    ,
        @NamedQuery(name = "CampaignAction.findByCreatedAt",
            query = "SELECT c FROM CampaignAction c WHERE c.createdAt = :createdAt")
    ,
        @NamedQuery(name = "CampaignAction.findByUpdatedAt",
            query = "SELECT c FROM CampaignAction c WHERE c.updatedAt = :updatedAt")
    ,
        @NamedQuery(name = "CampaignAction.findByCampaignId",
            query = "SELECT c FROM CampaignAction c WHERE c.campaignId = :campaignId")
    ,
        @NamedQuery(name = "CampaignAction.findByGroupId",
            query = "SELECT c FROM CampaignAction c WHERE c.groupId = :groupId")
    ,
        @NamedQuery(name = "CampaignAction.findByTotalContact",
            query = "SELECT c FROM CampaignAction c WHERE c.totalContact = :totalContact")
    ,
        @NamedQuery(name = "CampaignAction.findByTotalCall",
            query = "SELECT c FROM CampaignAction c WHERE c.totalCall = :totalCall")
    ,
        @NamedQuery(name = "CampaignAction.findByStatus",
            query = "SELECT c FROM CampaignAction c WHERE c.status = :status")
    ,
        @NamedQuery(name = "CampaignAction.findByDateStart",
            query = "SELECT c FROM CampaignAction c WHERE c.dateStart = :dateStart")
    ,
        @NamedQuery(name = "CampaignAction.findByUid",
            query = "SELECT c FROM CampaignAction c WHERE c.uid = :uid")
    ,
        @NamedQuery(name = "CampaignAction.findByDateFinish",
            query = "SELECT c FROM CampaignAction c WHERE c.dateFinish = :dateFinish")
    ,
        @NamedQuery(name = "CampaignAction.findByTimeOfDayStart",
            query = "SELECT c FROM CampaignAction c WHERE c.timeOfDayStart = :timeOfDayStart")
    ,
        @NamedQuery(name = "CampaignAction.findByTimeOfDayStop",
            query = "SELECT c FROM CampaignAction c WHERE c.timeOfDayStop = :timeOfDayStop")
    ,
        @NamedQuery(name = "CampaignAction.findBySuccessCall",
            query = "SELECT c FROM CampaignAction c WHERE c.successCall = :successCall")
    ,
        @NamedQuery(name = "CampaignAction.findByUnSuccessCall",
            query = "SELECT c FROM CampaignAction c WHERE c.unSuccessCall = :unSuccessCall")
    ,
        @NamedQuery(name = "CampaignAction.findByFinishedAt",
            query = "SELECT c FROM CampaignAction c WHERE c.finishedAt = :finishedAt")
    ,
        @NamedQuery(name = "CampaignAction.findByCusId",
            query = "SELECT c FROM CampaignAction c WHERE c.cusId = :cusId")})
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class CampaignAction extends BaseEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Column(name = "campaign_id")
    private Integer campaignId;
    @Column(name = "group_id")
    private Long groupId;
    @Column(name = "total_contact")
    private Integer totalContact;
    @Column(name = "total_call")
    private Integer totalCall;
    @Column(name = "status")
    private Integer status;
    @Column(name = "date_start")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateStart;
    @Column(name = "uid")
    private Integer uid;
    @Column(name = "date_finish")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFinish;
    @Size(max = 50)
    @Column(name = "time_of_day_start")
    private String timeOfDayStart;
    @Size(max = 50)
    @Column(name = "time_of_day_stop")
    private String timeOfDayStop;
    @Column(name = "success_call")
    private Integer successCall;
    @Column(name = "un_success_call")
    private Integer unSuccessCall;
    @Column(name = "finished_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishedAt;
    @Column(name = "cus_id")
    private Integer cusId;
    @Column(name = "call_dates")
    @Convert(converter = ListConverter.class)
    private List<String> callDate;

    public enum STATUS {
        Draff(0), Running(1), Stop(2), Finish(3), error(-1), Deleted(-2), ForceReload(-3);
        int status;

        private STATUS(int status) {
            this.status = status;
        }

        public int get() {
            return status;
        }
    }

}

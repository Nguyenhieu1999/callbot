package com.its.callbot.commons.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;

@MappedSuperclass
public class BaseEntity implements Serializable{

	@Column(name = "created_at")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date createdAt = new Date();
	@Column(name = "updated_at")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date updatedAt = new Date();
        
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@PrePersist
	public void prePersist() {
		createdAt = new Date();
		updatedAt = new Date();
//		this.version = 0;
	}

	@PreUpdate
	public void preUpdate() {
		updatedAt = new Date();
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.entities;

import com.its.callbot.commons.entities.converter.PhoneNoToISDNConverter;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author quangdt
 */
@Getter
@Setter
@MappedSuperclass
public abstract class SpecialCustomer extends BaseEntity{
    @Basic(optional = false)
    @NotNull
    @Column(name = "enable")
    private boolean enable;
    @Size(max = 45)
    @Column(name = "owner")
    private String owner;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "isdn")
    @Convert(converter = PhoneNoToISDNConverter.class)
    private String isdn;
    @Basic
    @Column(name = "campaign_id")
    private Integer campaignId;
}

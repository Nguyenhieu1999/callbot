package com.its.callbot.commons.repositories;

import com.its.callbot.commons.entities.CallLogEntity;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation=Propagation.REQUIRED, readOnly=true, noRollbackFor=Exception.class)
public interface CallLogRepository extends BaseRepository<CallLogEntity, Long> {

	@Query("SELECT c FROM CallLogEntity c WHERE c.botConversationId = :conversationId")
	Page<CallLogEntity> findByBotConversationId(@Param("conversationId")String conversationId, Pageable pageable);

	@Query("SELECT c FROM CallLogEntity c WHERE c.campaignId = :campaignId AND c.contactId = :contactId")
	Optional<CallLogEntity> findByCampaignIdAndContactId(@Param("campaignId")Integer campaignId, @Param("contactId")Long contactId);
        
        @Query("SELECT c FROM CallLogEntity c WHERE c.campaignActionId = :campaignActionId AND c.contactId = :contactId")
	Optional<CallLogEntity> findByCampaignActionIdAndContactId(@Param("campaignActionId")Long campaignActionId, @Param("contactId")Long contactId);

        
	@Query("SELECT c FROM CallLogEntity c WHERE c.callStatus in :callStatus AND c.campaignActionId = :campaignActionId AND c.createdAt < :allowedTime")
	List<CallLogEntity> findRetryRecord(@Param("callStatus")List<Integer> callStatus, @Param("campaignActionId")Long campaignActionId, @Param("allowedTime")Date allowedTime);


}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.repositories;

import com.its.callbot.commons.entities.CampaignAction;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author quangdt
 */
@Repository
@Transactional(propagation=Propagation.REQUIRED, readOnly=true, noRollbackFor=Exception.class)
public interface CampaignActionRepository extends BaseRepository<CampaignAction, Long>{
    
	@Query("SELECT ca FROM CampaignAction ca WHERE ca.status IN :status")
	List<CampaignAction> findByListStatus(@Param("status") List<Integer> status);
    
    	@Query("SELECT ca FROM CampaignAction ca WHERE ca.status = 1 "
                + "and ca.campaignId = :campaignId and ca.dateStart < now() and now() < ca.dateFinish")
	List<CampaignAction> findRunningByCampaignId(@Param("campaignId") Integer campaignId);

    
}

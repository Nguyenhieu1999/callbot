/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.config;

import com.its.callbot.callmanage.thresold.CCUCounter;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.lang.Nullable;
import org.springframework.scheduling.annotation.Scheduled;

/**
 *
 * @author quangdt
 */
@Configuration
@Log4j2
public class CallManager {
    @Autowired
    ApplicationEventPublisher applicationEventPublisher;
    static final Map<Integer, CCUCounter> campaignCCUAtomic = new ConcurrentHashMap<>();
    
    public static final CCUCounter ccuCounter = new CCUCounter(0, null);
    
    /**
     * Get Campaign CCU Counter from map. If not exist, create new.
     * @param campaignId
     * @return 
     */
    @Bean("Campaign_CCU_Counter")
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public CCUCounter getOrCreateCampaignCCUCounter(int campaignId){
        CCUCounter campaignCCUCounter = null;
        synchronized(campaignCCUAtomic){
            campaignCCUCounter = campaignCCUAtomic.get(campaignId);
            if(campaignCCUCounter == null){
                log.info("Create CCU Counter for campaign {}", campaignId);
                campaignCCUCounter = new CCUCounter(campaignId, applicationEventPublisher);
                campaignCCUAtomic.put(campaignId, campaignCCUCounter);
            }
        }
        return campaignCCUCounter;
    }
    /**
     * Get CampaignCCU Counter from map. If not exist, return null.
     * @param campaignId
     * @return 
     */
    @Nullable
    public static CCUCounter getCampaignCCUCounter(Integer campaignId){
        return campaignCCUAtomic.get(campaignId);
    }
    
    @Nullable
    public static CCUCounter removeCampaignCCUCounterIfEmpty(Integer campaignId){
        synchronized(campaignCCUAtomic){
            CCUCounter counter = campaignCCUAtomic.get(campaignId);
            if(counter != null && counter.get() == 0){
                return campaignCCUAtomic.remove(campaignId);
            }else{
                return null;
            }
        }
    }
    
    @Scheduled(fixedRate = 60 * 1000, initialDelay = 1000)
    public void dumpCCU(){
        log.info("++++++++++{}",ccuCounter);
        Collection<CCUCounter> lstCCUCounter = campaignCCUAtomic.values();
        try{
            for(CCUCounter ccuCounter : lstCCUCounter){
                try {
                    log.info("+++++++++++++++{}",ccuCounter);
                } catch (Exception e) {
                }
            }
        }catch(Exception e){
            
        }
    }
}

package com.its.callbot.commons.repositories;

import com.its.callbot.commons.entities.CampaignBotMappingField;

public interface CampaignBotMappingFieldRepository extends BaseRepository<CampaignBotMappingField, String> {

}

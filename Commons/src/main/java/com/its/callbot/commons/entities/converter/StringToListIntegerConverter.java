/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.entities.converter;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author quangdt
 */
@Converter
public class StringToListIntegerConverter implements
        AttributeConverter<List<Integer>, String> {

    @Override
    public String convertToDatabaseColumn(List<Integer> attribute) {
        if (attribute == null) {
            return null;
        }
        if (attribute.isEmpty()) {
            return "";
        }
        return attribute.toString().replace("[", "").replace("]", "");
    }

    @Override
    public List<Integer> convertToEntityAttribute(String columnData) {
        if (null == columnData) {
            return null;
        }
        columnData = columnData.replace("[", "").replace("]", "");
        String[] temp = columnData.split(",");
        List lst = new ArrayList(temp.length);
        for (String t : temp) {
            if (t != null && !t.trim().isEmpty()) {
                lst.add(Integer.parseInt(t.trim()));
            }
        }
        return lst;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.utils;

import java.util.Base64;
import java.util.UUID;
import org.springframework.stereotype.Component;

/**
 *
 * @author FOCUS
 */
@Component
public class CommonUtils {

    public String base64Encode(String text) {
        return Base64.getEncoder().encodeToString(text.getBytes());
    }

    public String base64Decode(String encoded) {
        return new String(Base64.getDecoder().decode(encoded));
    }

    public Boolean compare(String a, String b) {
        return a.equals(b);
    }

    public String generateToken() {
        return UUID.randomUUID().toString();
    }
}

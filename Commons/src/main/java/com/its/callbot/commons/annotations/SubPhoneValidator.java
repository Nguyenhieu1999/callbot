package com.its.callbot.commons.annotations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.apache.commons.validator.routines.RegexValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;


public class SubPhoneValidator implements ConstraintValidator<Annotation.SubPhoneNumber, String> {

    @Value(value = "${subscriber.phone.pattern}")
    private String mobilePattern;

    public SubPhoneValidator() {
    }

    @Override
    public void initialize(Annotation.SubPhoneNumber constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (StringUtils.isEmpty(value)) {
            return true;
        } else {
            String template = context.getDefaultConstraintMessageTemplate();
            boolean isValid = true;
            RegexValidator regexValidator = new RegexValidator(mobilePattern);
            if (value.length() > 15) {
                isValid = false;
                template = "ERR_0903";
            } else if (!regexValidator.isValid(value)) {
                isValid = false;
                template = "ERR_0911";
            }

            if (!isValid) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(template).addConstraintViolation();
            }

            return isValid;
        }
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

/**
 *
 * @author FOCUS
 */
@Component
@Log4j2
public class DateFormater {

    final String[] DATE_FORMAT = new String[]{"dd/MM/yyyy"};
    private final SimpleDateFormat sdf = new SimpleDateFormat();
    private static final DateFormat parseFormat = new SimpleDateFormat("dd/MM/yy");
    private static final DateFormat formattingFormat = new SimpleDateFormat("dd/MM/yyyy");

    public Date format(String input) {
        for (String format : DATE_FORMAT) {
            try {
                sdf.applyPattern(format);
                Date d = sdf.parse(input);
                return d;
            } catch (Exception e) {
            }
        }

        log.warn("can't parse time input:" + input);
        return null;
    }

    public Date specialFormat(String input) {
        try {
            Date d = parseFormat.parse(input);
            return formattingFormat.parse(formattingFormat.format(d));
        } catch (Exception e) {
        }

        log.warn("can't parse time input:" + input);
        return null;
    }
}

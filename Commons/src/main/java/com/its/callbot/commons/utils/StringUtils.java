/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

/**
 *
 * @author FOCUS
 */
@Component
public class StringUtils {

    public final String BILLION = "TỈ";
    public final String MILLION = "TRIỆU";

    public String getTextBetween(String source, String start, String end) {
        Pattern p = Pattern.compile(start + "(.*)" + end);
        Matcher m = p.matcher(source);

        if (m.find()) {
            String result = m.group(1);
            return result;
        } else {
            return null;
        }
    }

    public String getStringNumber(String source) {
        return source.replaceAll("[^\\d]", "");
    }

    public String getPriceFromText(String input) {
        input = input.toUpperCase();
        long value = 0;
        if (input.contains(BILLION)) {
            String[] i = input.split(BILLION);
            value += Integer.valueOf(i[0].trim()) * 1000000000;

            if (i[1].contains(MILLION)) {
                String[] k = i[1].split(MILLION);
                value += (Integer.valueOf(k[0].trim()) * 1000000);
            }
        } else {
            if (input.contains(MILLION)) {
                String[] k = input.split(MILLION);
                value += (Integer.valueOf(k[0].trim()) * 1000000);
            }
        }

        return String.valueOf(value);
    }
}

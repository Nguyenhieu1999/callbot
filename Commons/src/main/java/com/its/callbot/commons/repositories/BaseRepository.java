/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.repositories;

import com.its.callbot.commons.entities.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 *
 * @author quangdt
 * @param <T>
 * @param <P>
 */
@NoRepositoryBean
public interface BaseRepository<T extends BaseEntity, P> extends JpaRepository<T, P>{
    
}

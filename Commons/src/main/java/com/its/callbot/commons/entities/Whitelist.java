package com.its.callbot.commons.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author quangdt
 */
@Entity
@Table(name = "whitelist")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Whitelist.findAll", query = "SELECT w FROM Whitelist w where w.enable = TRUE")
    ,
        @NamedQuery(name = "Whitelist.findByIsdn",
            query = "SELECT w FROM Whitelist w WHERE w.isdn = :isdn and w.enable = TRUE")
    ,@NamedQuery(name = "Blacklist.findByCampaignId",
            query = "SELECT b FROM Blacklist b WHERE b.campaignId = :campaignId and b.enable = TRUE")})
@Getter
@Setter
@ToString
public class Whitelist extends SpecialCustomer implements Serializable {

}

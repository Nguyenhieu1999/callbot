/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.repositories;

import com.its.callbot.commons.entities.Blacklist;
import java.util.List;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author quangdt
 */
public interface BlackListRepository extends BaseRepository<Blacklist, String>{
    public List<Blacklist> findByCampaignId(@Param("campaignId") Integer campaignID);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.services;

import com.its.callbot.commons.repositories.ConfigRepository;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author quangdt
 */
@Getter
@Log4j2
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class ConfigService {
    String nationalCode;
    
    @Autowired
    protected ConfigRepository repo;
    
    @PostConstruct
    public void init(){
        nationalCode = repo.findByKey("national.code").getValue();
    }
}

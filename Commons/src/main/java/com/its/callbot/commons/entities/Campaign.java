/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.entities;

import com.its.callbot.commons.entities.converter.ListConverter;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author quangdt
 */
@Entity
@Table(name = "campaign")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Campaign.findAll", query = "SELECT c FROM Campaign c")
    ,
        @NamedQuery(name = "Campaign.findById",
            query = "SELECT c FROM Campaign c WHERE c.id = :id")
    ,
        @NamedQuery(name = "Campaign.findByName",
            query = "SELECT c FROM Campaign c WHERE c.name = :name")
    ,
        @NamedQuery(name = "Campaign.findByStatus",
            query = "SELECT c FROM Campaign c WHERE c.status = :status")
    ,
        @NamedQuery(name = "Campaign.findByCreatedAt",
            query = "SELECT c FROM Campaign c WHERE c.createdAt = :createdAt")
    ,
        @NamedQuery(name = "Campaign.findByUpdatedAt",
            query = "SELECT c FROM Campaign c WHERE c.updatedAt = :updatedAt")
    ,
        @NamedQuery(name = "Campaign.findUpdated",
            query = "SELECT c FROM Campaign c WHERE c.updatedAt > :updatedAt order by c.updatedAt desc")
    ,
        @NamedQuery(name = "Campaign.findByDeletedAt",
            query = "SELECT c FROM Campaign c WHERE c.deletedAt = :deletedAt")
    
    ,
        @NamedQuery(name = "Campaign.findByBotId",
            query = "SELECT c FROM Campaign c WHERE c.botId = :botId")
    ,
        @NamedQuery(name = "Campaign.findByRetryAfter",
            query = "SELECT c FROM Campaign c WHERE c.retryAfter = :retryAfter")
    ,
        @NamedQuery(name = "Campaign.findByRetryTime",
            query = "SELECT c FROM Campaign c WHERE c.retryTime = :retryTime")
    ,
        @NamedQuery(name = "Campaign.findByConcurrentCall",
            query = "SELECT c FROM Campaign c WHERE c.concurrentCall = :concurrentCall")
    ,
        @NamedQuery(name = "Campaign.findByBotRegion",
            query = "SELECT c FROM Campaign c WHERE c.botRegion = :botRegion")
    ,
        @NamedQuery(name = "Campaign.findByHotline",
            query = "SELECT c FROM Campaign c WHERE c.hotline = :hotline")
    ,
        @NamedQuery(name = "Campaign.findByCusId",
            query = "SELECT c FROM Campaign c WHERE c.cusId = :cusId")})
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class Campaign extends BaseEntity {

   

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "status")
    private Integer status;
    
    @Column(name = "deleted_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @Column(name = "bot_id")
    private Integer botId;
    @Column(name = "retry_after")
    private Integer retryAfter;
    @Column(name = "retry_time")
    private Integer retryTime;
    @Column(name = "concurrent_call")
    private Integer concurrentCall;
    @Column(name = "cus_id")
    private Integer cusId;
    
    @Size(max = 250)
    @Column(name = "name")
    private String name;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Size(max = 50)
    @Column(name = "bot_region")
    private String botRegion;
    @Size(max = 50)
    @Column(name = "hotline")
    private String hotline;
    @Column(name = "date_start")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateStart;
    @Column(name = "date_finish")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFinish;
    @Size(max = 50)
    @Column(name = "time_of_day_start")
    private String timeOfDayStart;
    @Size(max = 50)
    @Column(name = "time_of_day_stop")
    private String timeOfDayStop;
    
    @Column(name = "call_dates")
    @Convert(converter = ListConverter.class)
    private List<String> callDate;
    
    public boolean validDate(int date){
        String sDate = String.valueOf(date);
        return callDate.contains(sDate);
    }
    
    public boolean isRunning(){
        return STATUS.RUNNING.getValue() == status;
    }
    
    
    public static enum STATUS{
        DRAFF(0), RUNNING(1), STOP(2),FINISH(3), ERROR(-1), DELETED(-2);
        int status;
        private STATUS(int status){
            this.status = status;
        }
        public int getValue(){
            return status;
        }
        
        public static STATUS valueOf(int status){
            switch(status){
                case 0:
                    return DRAFF;
                case 1:
                    return RUNNING;
                case 2:
                    return STOP;
                case 3:
                    return FINISH;
                case -1: 
                    return ERROR;
                case -2:
                    return DELETED;
                default:
                    return null;
            }
        }
        
        public static String nameOf(int status){
            switch(status){
                case 0:
                    return DRAFF.name();
                case 1:
                    return RUNNING.name();
                case 2:
                    return STOP.name();
                case 3:
                    return FINISH.name();
                case -1: 
                    return ERROR.name();
                case -2:
                    return DELETED.name();
                default:
                    return "UNKNOW";
            }
        }
    }

}

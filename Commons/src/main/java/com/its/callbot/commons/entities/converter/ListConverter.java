/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.entities.converter;

import java.util.Arrays;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author quangdt
 */
@Converter
public class ListConverter implements AttributeConverter<List<String>, String>{

    @Override
    public String convertToDatabaseColumn(List<String> attribute) {
        if(attribute == null){
            return null;
        }
        if(attribute.isEmpty()){
            return "";
        }
        return attribute.toString().replace("[", "").replace("]", "");
    }

    @Override
    public List<String> convertToEntityAttribute(String columnData) {
        if(null == columnData){
            return null;
        }
        String[] temp = columnData.split(",");
        return Arrays.asList(temp);
    }

}

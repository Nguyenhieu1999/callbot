/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author quangdt
 */
@Entity
@Table(name = "campaign_action_log")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CampaignActionLog.findAll",
            query = "SELECT c FROM CampaignActionLog c")
    ,
        @NamedQuery(name = "CampaignActionLog.findById",
            query = "SELECT c FROM CampaignActionLog c WHERE c.id = :id")
    ,
        @NamedQuery(name = "CampaignActionLog.findByCampaignId",
            query = "SELECT c FROM CampaignActionLog c WHERE c.campaignId = :campaignId")
    ,
        @NamedQuery(name = "CampaignActionLog.findByStatus",
            query = "SELECT c FROM CampaignActionLog c WHERE c.status = :status")
    ,
        @NamedQuery(name = "CampaignActionLog.findByCampaignActionId",
            query = "SELECT c FROM CampaignActionLog c WHERE c.campaignActionId = :campaignActionId")
    ,
        @NamedQuery(name = "CampaignActionLog.findByCreatedAt",
            query = "SELECT c FROM CampaignActionLog c WHERE c.createdAt = :createdAt")
    ,
        @NamedQuery(name = "CampaignActionLog.findByReason",
            query = "SELECT c FROM CampaignActionLog c WHERE c.reason = :reason")
    ,
        @NamedQuery(name = "CampaignActionLog.findByUid",
            query = "SELECT c FROM CampaignActionLog c WHERE c.uid = :uid")
    ,
        @NamedQuery(name = "CampaignActionLog.findByUpdatedAt",
            query = "SELECT c FROM CampaignActionLog c WHERE c.updatedAt = :updatedAt")
    ,
        @NamedQuery(name = "CampaignActionLog.findByCusId",
            query = "SELECT c FROM CampaignActionLog c WHERE c.cusId = :cusId")})
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class CampaignActionLog extends BaseEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "campaign_id")
    private long campaignId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private int status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "campaign_action_id")
    private long campaignActionId;
    
    @Size(max = 50)
    @Column(name = "reason")
    private String reason;
    @Column(name = "uid")
    private Integer uid;
   
    @Column(name = "cus_id")
    private Integer cusId;

    public enum STATUS{
        Draff(0),Running(1), Stop(2), Finish(3), error(-1), Deleted(-2);
        int status;
        private STATUS(int status){
            this.status = status;
        }
        
        public int get(){
            return status;
        }
    }
}

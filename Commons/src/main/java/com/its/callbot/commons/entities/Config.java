/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.naming.ConfigurationException;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Transient;

/**
 *
 * @author quangdt
 * @param <T>
 */
@Entity
@Table(name = "config")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Config.findAll", query = "SELECT c FROM Config c")
    ,
    @NamedQuery(name = "Config.findByKey",
            query = "SELECT c from Config c where c.key =:key")})
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Config implements Serializable {

    @Id
    @Column(name = "param_name")
    private String key;
    @Column(name = "param_value")
    private String value;
    
    @Transient
    public boolean getBooleanValue() throws ConfigurationException {
        try {
            return Boolean.parseBoolean(value);
        } catch (Exception e) {
            throw new ConfigurationException(
                    "Can't get Boolean value from " + key + "(" + value + ")");
        }
    }
    @Transient
    public int getIntValue() throws ConfigurationException {
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new ConfigurationException(
                    "Can't get Integer value from " + key + "(" + value + ")");
        }
    }
    @Transient
    public long getLongValue() throws ConfigurationException {
        try {
            return Long.parseLong(value);
        } catch (Exception e) {
            throw new ConfigurationException(
                    "Can't get Long value from " + key + "(" + value + ")");
        }
    }
    @Transient
    public List<Integer> getListIntValue() throws ConfigurationException {
        try {
            String[] temp = value.split(";");
            List lst = new ArrayList(temp.length);
            for (String t : temp) {
                if (t != null && !t.trim().isEmpty()) {
                    lst.add(Integer.parseInt(t.trim()));
                }
            }
            return lst;
        } catch (Exception e) {
            throw new ConfigurationException(
                    "Can't get List<Integer> value from " + key + "(" + value + ")");
        }
    }
}

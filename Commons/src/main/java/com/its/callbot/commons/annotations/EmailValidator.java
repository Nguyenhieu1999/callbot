package com.its.callbot.commons.annotations;

import org.apache.logging.log4j.util.Strings;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EmailValidator implements ConstraintValidator<Annotation.Email, String> {


    private int size;

    public EmailValidator() {
    }

    @Override
    public void initialize(Annotation.Email constraintAnnotation) {
        this.size = constraintAnnotation.size();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (Strings.isBlank(value)) {
            return true;
        } else {
            String message = "ERR_0814";
            boolean isValid;
            if (value.length() > this.size) {
                message = "ERR_0803";
                isValid = false;
            } else {
                org.apache.commons.validator.routines.EmailValidator emailValidator = org.apache.commons.validator.routines.EmailValidator.getInstance();
                isValid = emailValidator.isValid(value);
            }

            if (!isValid) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
            }

            return isValid;
        }
    }
}


package com.its.callbot.commons.annotations;

import com.its.callbot.commons.utils.DateTimeUtil;
import org.apache.logging.log4j.util.Strings;
import org.apache.commons.validator.routines.DateValidator;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeValidator implements ConstraintValidator<Annotation.DateTime, String> {

    

    private String dateFormat;
    private boolean past;
    private boolean future;

    @Override
    public void initialize(Annotation.DateTime constraintAnnotation) {
        this.dateFormat = constraintAnnotation.dateFormat();
        this.past = constraintAnnotation.past();
        this.future = constraintAnnotation.future();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (Strings.isBlank(value)) {
            return true;
        } else {
            String template = constraintValidatorContext.getDefaultConstraintMessageTemplate();

            boolean isValid;
            try {
//                if ("HH:mm".equals(this.dateFormat)) {
//                    isValid = DateTimeUtil.isValidTime(value);
//                } else {
                    isValid = DateValidator.getInstance().isValid(value, this.dateFormat);
//                }

                if (isValid) {
                    DateFormat df = new SimpleDateFormat(this.dateFormat);
                    Date dt = df.parse(value);
                    Date currentDt = new Date();
                    if (this.past && currentDt.before(dt)) {
                        isValid = false;
                        template = "ERR_0812";
                    }

                    if (this.future && currentDt.after(dt)) {
                        isValid = false;
                        template = "ERR_0813";
                    }
                } else {
                    template = "ERR_0811";
                }
            } catch (Exception var8) {
                isValid = false;
                template = "ERR_0807";
            }

            if (!isValid) {
                constraintValidatorContext.disableDefaultConstraintViolation();
                constraintValidatorContext.buildConstraintViolationWithTemplate(template).addConstraintViolation();
            }

            return isValid;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.repositories;

import com.its.callbot.commons.entities.Config;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

/**
 *
 * @author quangdt
 */
@Component
public interface ConfigRepository extends JpaRepository<Config, Integer>{
    
    public Config findByKey(@Param("key") String key);
    
    @Query("select c from Config c "
    		+ "    where "
    		+ "        lower(c.key) like CONCAT('%', :key)")
    public List<Config> findAllByKeyStartWith(@Param("key") String key);
}

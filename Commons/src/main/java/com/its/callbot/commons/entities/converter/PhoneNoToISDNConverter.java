/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.entities.converter;

import com.its.callbot.commons.services.ConfigService;
import dtsoft.utils.MSISDN;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author quangdt
 */
@Converter
public class PhoneNoToISDNConverter implements AttributeConverter<String, String>{

    @Autowired
    ConfigService config;
    
    @Override
    public String convertToDatabaseColumn(String x) {
        return x;
    }

    @Override
    public String convertToEntityAttribute(String y) {
        return MSISDN.toIsdn(y, config.getNationalCode());
    }
    
}

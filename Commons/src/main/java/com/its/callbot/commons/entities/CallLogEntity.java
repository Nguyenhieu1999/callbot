package com.its.callbot.commons.entities;

import java.util.Date;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The persistent class for the call_logs database table.
 *
 */
@Entity
@Table(name = "call_logs")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class CallLogEntity extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Long id;

    @Column(name = "audio_url")
    private String audioUrl;

    @Lob
    @Column(name = "bot_conversation_text")
    private String botConversatioText;

    @Lob
    @Column(name = "bot_conversation_detail")
    private String botConversatioDetail;

    @Column(name = "bot_conversation_id")
    private String botConversationId;

    @Column(name = "call_status")
    private Integer callStatus;

    private String called;

    private String caller;

    @Column(name = "campaign_action_id")
    private Long campaignActionId;

    @Column(name = "campaign_id")
    private Integer campaignId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    private Date updatedAt;

    @Column(name = "cus_id")
    private Integer cusId;

    @Column(name = "retried_times")
    private Integer retriedTimes;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_time")
    private Date endTime;

    @Column(name = "ring_duration")
    private Long ringDuration;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_time")
    private Date startTime;

    @Column(name = "talk_duration")
    private Long talkDuration;
    
    @Column(name = "servey_status")
    private Integer serveyStatus;
    
    @Column(name = "contact_Id")
    private Integer contactId;
    
    public CallLogEntity(Long id, Integer serveyStatus, Integer campaignId, 
            Integer callStatus, String called, Date endTime ) {
        this.id = id;
        this.callStatus = callStatus;
        this.called = called;
        this.campaignId = campaignId;
        this.endTime = endTime;
        this.serveyStatus = serveyStatus;
    }
    
    
    public static synchronized CallLogEntity generate(CampaignAction campaign,
            String msisdn, String conversId, int callStatus) {
        Date today = new Date();
        return CallLogEntity.builder().botConversationId(conversId)
                .campaignActionId(campaign.getId())
                .campaignId(campaign.getCampaignId())
                .called(msisdn)
                .ringDuration(0L)
                .startTime(today)
                .endTime(today)
                .talkDuration(0L)
                .updatedAt(today)
                .createdAt(today)
                .callStatus(callStatus).retriedTimes(0)
                .serveyStatus(ServeyStatus.NEW.get())
                .build();
    }

    public static enum Status {

        CREATED(0), REQUEST_CALLOUT_FAIL(-100), SUCCESS(100), SUB_HANGUP(101), CALL_FORWARD(
                102), MISSCALL(103),
        UNAVAILABLE(104), SYSTEM_ERROR(105), ALREADY_PAID(300), BLACKLIST(301);

        private int value;

        private Status(int v) {
            this.value = v;
        }

        public static Status findByValue(int input) {
            for (Status type : values()) {
                if (type.value == input) {
                    return type;
                }
            }
            return null;
        }
        
        public int get(){
            return this.value;
        }
    }
    public static enum ServeyStatus {

        NEW(0), IN_PROCESS(1), BLOCKED(300), SUCCESS(200), FAIL(201), ERROR(400), OVERTIME(500);

        private int value;

        private ServeyStatus(int v) {
            this.value = v;
        }

        public static ServeyStatus findByValue(int input) {
            for (ServeyStatus type : values()) {
                if (type.value == input) {
                    return type;
                }
            }
            return null;
        }
        
        public int get(){
            return this.value;
        }
    }
}

package com.its.callbot.commons.annotations;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class EnumValidator implements ConstraintValidator<Annotation.Enumerated, String> {


    private boolean allowBlank;
    private boolean ignoreCaseSensitive;
    private List<String> enums;

    public EnumValidator() {
    }

    @Override
    public void initialize(Annotation.Enumerated constraintAnnotation) {
        this.enums = new ArrayList();
        this.allowBlank = constraintAnnotation.allowBlank();
        this.ignoreCaseSensitive = constraintAnnotation.ignoreCaseSensitive();
        Class<? extends Enum<?>> enumClass = constraintAnnotation.enumClazz();
        Enum[] enumValArr = (Enum[])enumClass.getEnumConstants();
        Enum[] var4 = enumValArr;
        int var5 = enumValArr.length;

        for(int var6 = 0; var6 < var5; ++var6) {
            Enum enumVal = var4[var6];
            this.enums.add(enumVal.name());
        }

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        boolean isValid = true;
        String template = context.getDefaultConstraintMessageTemplate();
        if (Strings.isNotBlank(value)) {
            if (this.ignoreCaseSensitive) {
                value = value.toUpperCase();
            }

            if (!this.enums.contains(value)) {
                isValid = false;
                template = "ERR_0815";
            }
        } else if (!this.allowBlank) {
            isValid = false;
            template = "ERR_0801";
        }

        if (!isValid) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(template).addConstraintViolation();
        }

        return isValid;
    }
}

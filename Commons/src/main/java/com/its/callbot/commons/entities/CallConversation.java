/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.commons.entities;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author quangdt
 */
@Entity
@Table(name = "call_queue")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "CallConversation.findAll", query = "SELECT c FROM CallConversation c"),
		@NamedQuery(name = "CallConversation.findById", query = "SELECT c FROM CallConversation c WHERE c.id = :id"),
		@NamedQuery(name = "CallConversation.findByCampaignActionId", query = "SELECT c FROM CallConversation c WHERE c.campaignAction = :campaignActionId"),
		@NamedQuery(name = "CallConversation.findByConversationId", query = "SELECT c FROM CallConversation c WHERE c.conversationId = :conversationId") })
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CallConversation extends BaseEntity {
	@Id
	@Basic(optional = false)
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	@JoinColumn(name = "campaign_action_id", referencedColumnName = "id")
	@ManyToOne()
	private CampaignAction campaignAction;
	@JoinColumn(name = "campaign_id", referencedColumnName = "id")
	@ManyToOne()
	private Campaign campaign;
	@Column(name = "isdn")
	private String isdn;
	@Size(max = 500)
	@Column(name = "conversation_id")
	private String conversationId;
	@Column(name = "status")
	private int status;
	@Column(name = "retry")
	private Integer retry;

	@Column(name = "bot_id")
	private Integer botId;
	@Column(name = "bot_region")
	private String botRegion;
	@Column(name = "hotline")
	private String hotline;
	@Column(name = "bot_url")
	private String botUrl;
	@Column(name = "name")
	private String name;
	@Column(name = "cus_id")
	private String cusId;
	@Column(name = "email")
	private String email;
	@Column(name = "address")
	private String address;
	@Column(name = "gender")
	private String gender;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dob")
	private Date dob;
	@Column(name = "customer_field1")
	private String customerField1;
	@Column(name = "customer_field2")
	private String customerField2;
	@Column(name = "customer_field3")
	private String customerField3;
	@Column(name = "customer_field4")
	private String customerField4;
	@Column(name = "customer_field5")
	private String customerField5;
	@Column(name = "customer_field6")
	private String customerField6;
	@Column(name = "customer_field7")
	private String customerField7;
	@Column(name = "customer_field8")
	private String customerField8;
	@Column(name = "customer_field9")
	private String customerField9;
	@Column(name = "customer_field10")
	private String customerField10;

//    xxxxxxxx

//    private Map<String, String> inputslots;

	public static final int STATUS_INIT = -1;
	public static final int STATUS_NEW = 0;
	public static final int STATUS_CALLING = 1;
	public static final int STATUS_CALLED = 2;
	public static final int STATUS_ERR = 3;

	public static final int STATUS_ALREADY_PAID = 300;
	public static final int STATUS_BLACKLIST = 301;
	public static final int STATUS_CALL_FAIL = 400;
	public static final int STATUS_CONTACT_NOT_FOUND = 404;

	public static final String C_ID = "id";
	public static final String C_ISDN = "isdn";
	public static final String C_CONVERSATION_ID = "conversation_id";
	public static final String C_STATUS = "status";
	public static final String C_RETRY = "retry";
	public static final String C_BOT_ID = "bot_id";
	public static final String C_BOT_REGION = "bot_region";
	public static final String C_HOTLINE = "hotline";
	public static final String C_BOT_URL = "bot_url";

	public Map<String, String> getInputslots() {
		Map<String, String> getInputslots = new HashMap<>();
		getInputslots.put("code", "868412");
		return getInputslots;
	}
}

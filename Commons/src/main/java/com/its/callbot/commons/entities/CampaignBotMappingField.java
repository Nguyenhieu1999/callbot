package com.its.callbot.commons.entities;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigInteger;


/**
 * The persistent class for the campaign_bot_mapping_field database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@ToString
@Builder
@Entity
@Table(name="campaign_bot_mapping_field")
@NamedQuery(name="CampaignBotMappingField.findAll", query="SELECT c FROM CampaignBotMappingField c")
public class CampaignBotMappingField extends BaseEntity{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String id;

	@Column(name="bot_field")
	private String botField;

	@JoinColumn(name = "campaign_id", referencedColumnName = "id")
	@ManyToOne()
	private Campaign campaign;

	@Column(name="customer_field")
	private String customerField;
}
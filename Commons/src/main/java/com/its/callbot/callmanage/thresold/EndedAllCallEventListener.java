/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.thresold;


import com.its.callbot.commons.config.CallManager;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 *
 * @author quangdt
 */
@Component
@Log4j2
public class EndedAllCallEventListener implements
        ApplicationListener<EndedAllCallEvent> {

    @Override
    public void onApplicationEvent(EndedAllCallEvent e) {
        log.info("Handle {}", e);
        int campaignId = e.getCampaignId();
        CCUCounter counter = CallManager.removeCampaignCCUCounterIfEmpty(
                campaignId);
        if (counter != null) {
            log.info("Removed CCU Counter campaign {}", campaignId);
        }

    }

}

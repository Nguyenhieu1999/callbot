/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.thresold;

import lombok.ToString;
import org.springframework.context.ApplicationEvent;

/**
 *
 * @author quangdt
 */
@ToString
public class EndedAllCallEvent extends ApplicationEvent {
    private int campaignId;
    private int currentCCU;
    public EndedAllCallEvent(Object source,int campaignId, int currentValue) {
        super(source);
        this.campaignId = campaignId;
        this.currentCCU = currentValue;
    }

    public int getCurrentCCU() {
        return currentCCU;
    }

    public int getCampaignId() {
        return campaignId;
    }
    
    
}

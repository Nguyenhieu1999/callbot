/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.thresold;

import java.util.concurrent.atomic.AtomicInteger;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationEventPublisher;

/**
 *
 * @author quangdt
 */
@Log4j2
//@Component
@AllArgsConstructor
public class CCUCounter {
    private final AtomicInteger atomic = new AtomicInteger();
    private final int campaignId;
    private final ApplicationEventPublisher applicationEventPublisher;

    
    
    /**
     * Gets the current value.
     *
     * @return the current value
     */
    public final int get() {
        return atomic.get();
    }
    /**
     * Atomically increments by one the current value.
     *
     * @return the updated value
     */
    public final int incrementAndGet() {
        log.debug("Increase CCU");
        return atomic.incrementAndGet();
    }
    
    /**
     * Atomically decrements by one the current value if this is positive (greater than 0). 
     * If current value is less than or equals 0, do nothing
     * @return the updated value
     */
    public final synchronized int decrementAndGet(){
        log.debug("Decrease CCU");
        int value = atomic.updateAndGet(i -> i > 0 ? i - 1 : i);
        if(value == 0 && null != applicationEventPublisher){
            log.info("Raise encall all event campaign {}", campaignId);
            EndedAllCallEvent endAllCallEvent = new EndedAllCallEvent(this, campaignId, value);
            applicationEventPublisher.publishEvent(endAllCallEvent);
        }
        return value;
    }

    @Override
    public String toString() {
        return "CCUCounter{campaignId=" + campaignId + ", CCU=" + atomic.get()  + '}';
    }
    
}

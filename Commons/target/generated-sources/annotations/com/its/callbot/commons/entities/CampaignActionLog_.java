package com.its.callbot.commons.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-02-02T16:36:12")
@StaticMetamodel(CampaignActionLog.class)
public class CampaignActionLog_ extends BaseEntity_ {

    public static volatile SingularAttribute<CampaignActionLog, String> reason;
    public static volatile SingularAttribute<CampaignActionLog, Integer> uid;
    public static volatile SingularAttribute<CampaignActionLog, Long> campaignActionId;
    public static volatile SingularAttribute<CampaignActionLog, Integer> cusId;
    public static volatile SingularAttribute<CampaignActionLog, Long> campaignId;
    public static volatile SingularAttribute<CampaignActionLog, Long> id;
    public static volatile SingularAttribute<CampaignActionLog, Integer> status;

}
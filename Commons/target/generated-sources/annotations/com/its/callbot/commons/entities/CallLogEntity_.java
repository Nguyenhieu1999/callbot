package com.its.callbot.commons.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-02-02T16:36:12")
@StaticMetamodel(CallLogEntity.class)
public class CallLogEntity_ extends BaseEntity_ {

    public static volatile SingularAttribute<CallLogEntity, Long> campaignActionId;
    public static volatile SingularAttribute<CallLogEntity, String> called;
    public static volatile SingularAttribute<CallLogEntity, Integer> contactId;
    public static volatile SingularAttribute<CallLogEntity, Integer> campaignId;
    public static volatile SingularAttribute<CallLogEntity, String> botConversatioText;
    public static volatile SingularAttribute<CallLogEntity, Date> createdAt;
    public static volatile SingularAttribute<CallLogEntity, String> audioUrl;
    public static volatile SingularAttribute<CallLogEntity, String> caller;
    public static volatile SingularAttribute<CallLogEntity, Integer> retriedTimes;
    public static volatile SingularAttribute<CallLogEntity, String> botConversationId;
    public static volatile SingularAttribute<CallLogEntity, Integer> cusId;
    public static volatile SingularAttribute<CallLogEntity, String> botConversatioDetail;
    public static volatile SingularAttribute<CallLogEntity, Integer> callStatus;
    public static volatile SingularAttribute<CallLogEntity, Long> ringDuration;
    public static volatile SingularAttribute<CallLogEntity, Date> startTime;
    public static volatile SingularAttribute<CallLogEntity, Long> id;
    public static volatile SingularAttribute<CallLogEntity, Date> endTime;
    public static volatile SingularAttribute<CallLogEntity, Long> talkDuration;
    public static volatile SingularAttribute<CallLogEntity, Integer> serveyStatus;
    public static volatile SingularAttribute<CallLogEntity, Date> updatedAt;

}
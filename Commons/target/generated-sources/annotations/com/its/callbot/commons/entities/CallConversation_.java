package com.its.callbot.commons.entities;

import com.its.callbot.commons.entities.Campaign;
import com.its.callbot.commons.entities.CampaignAction;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-02-02T16:36:12")
@StaticMetamodel(CallConversation.class)
public class CallConversation_ extends BaseEntity_ {

    public static volatile SingularAttribute<CallConversation, String> isdn;
    public static volatile SingularAttribute<CallConversation, String> botUrl;
    public static volatile SingularAttribute<CallConversation, String> hotline;
    public static volatile SingularAttribute<CallConversation, String> conversationId;
    public static volatile SingularAttribute<CallConversation, Campaign> campaign;
    public static volatile SingularAttribute<CallConversation, CampaignAction> campaignAction;
    public static volatile SingularAttribute<CallConversation, Long> id;
    public static volatile SingularAttribute<CallConversation, Integer> botId;
    public static volatile SingularAttribute<CallConversation, Integer> retry;
    public static volatile SingularAttribute<CallConversation, String> botRegion;
    public static volatile SingularAttribute<CallConversation, Integer> status;

}
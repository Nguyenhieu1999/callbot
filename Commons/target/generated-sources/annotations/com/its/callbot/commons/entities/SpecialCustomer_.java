package com.its.callbot.commons.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-02-02T16:36:12")
@StaticMetamodel(SpecialCustomer.class)
public abstract class SpecialCustomer_ extends BaseEntity_ {

    public static volatile SingularAttribute<SpecialCustomer, String> owner;
    public static volatile SingularAttribute<SpecialCustomer, String> isdn;
    public static volatile SingularAttribute<SpecialCustomer, Boolean> enable;
    public static volatile SingularAttribute<SpecialCustomer, Integer> campaignId;

}
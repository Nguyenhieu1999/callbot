package com.its.callbot.commons.entities;

import java.util.Date;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-02-02T16:36:12")
@StaticMetamodel(Campaign.class)
public class Campaign_ extends BaseEntity_ {

    public static volatile SingularAttribute<Campaign, List> callDate;
    public static volatile SingularAttribute<Campaign, String> hotline;
    public static volatile SingularAttribute<Campaign, Integer> retryTime;
    public static volatile SingularAttribute<Campaign, Date> dateFinish;
    public static volatile SingularAttribute<Campaign, Integer> concurrentCall;
    public static volatile SingularAttribute<Campaign, String> botRegion;
    public static volatile SingularAttribute<Campaign, String> timeOfDayStop;
    public static volatile SingularAttribute<Campaign, Date> createdAt;
    public static volatile SingularAttribute<Campaign, Date> deletedAt;
    public static volatile SingularAttribute<Campaign, Integer> retryAfter;
    public static volatile SingularAttribute<Campaign, Integer> cusId;
    public static volatile SingularAttribute<Campaign, Date> dateStart;
    public static volatile SingularAttribute<Campaign, String> name;
    public static volatile SingularAttribute<Campaign, Integer> id;
    public static volatile SingularAttribute<Campaign, Integer> botId;
    public static volatile SingularAttribute<Campaign, String> timeOfDayStart;
    public static volatile SingularAttribute<Campaign, Integer> status;
    public static volatile SingularAttribute<Campaign, Date> updatedAt;

}
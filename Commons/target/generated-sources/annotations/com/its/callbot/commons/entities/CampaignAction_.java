package com.its.callbot.commons.entities;

import java.util.Date;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-02-02T16:36:12")
@StaticMetamodel(CampaignAction.class)
public class CampaignAction_ extends BaseEntity_ {

    public static volatile SingularAttribute<CampaignAction, List> callDate;
    public static volatile SingularAttribute<CampaignAction, Integer> campaignId;
    public static volatile SingularAttribute<CampaignAction, Long> groupId;
    public static volatile SingularAttribute<CampaignAction, Integer> unSuccessCall;
    public static volatile SingularAttribute<CampaignAction, Date> dateFinish;
    public static volatile SingularAttribute<CampaignAction, Integer> totalCall;
    public static volatile SingularAttribute<CampaignAction, Date> finishedAt;
    public static volatile SingularAttribute<CampaignAction, Integer> totalContact;
    public static volatile SingularAttribute<CampaignAction, String> timeOfDayStop;
    public static volatile SingularAttribute<CampaignAction, Integer> uid;
    public static volatile SingularAttribute<CampaignAction, Date> dateStart;
    public static volatile SingularAttribute<CampaignAction, Integer> cusId;
    public static volatile SingularAttribute<CampaignAction, Long> id;
    public static volatile SingularAttribute<CampaignAction, String> timeOfDayStart;
    public static volatile SingularAttribute<CampaignAction, Integer> successCall;
    public static volatile SingularAttribute<CampaignAction, Date> updatedAt;
    public static volatile SingularAttribute<CampaignAction, Integer> status;

}
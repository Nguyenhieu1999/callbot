package com.its.callbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
@ComponentScan(basePackages = "com.its")
@SpringBootApplication
@EnableScheduling
@EnableAutoConfiguration
@EnableWebMvc

public class CallManageApplication {
	public static void main(String[] args) {
		SpringApplication.run(CallManageApplication.class, args);
	}
// @Bean
//	public CommandLineRunner run(ApplicationContext appContext) {
//	  return args -> {
//	
//	      String[] beans = appContext.getBeanDefinitionNames();
//	      Arrays.stream(beans).sorted().forEach(System.out::println);
//	  };
//	}
}

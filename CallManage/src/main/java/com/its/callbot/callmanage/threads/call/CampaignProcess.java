/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.threads.call;

import com.its.callbot.callmanage.services.CampaignActionServices;
import com.its.callbot.commons.entities.Campaign;
import com.its.callbot.commons.entities.CampaignAction;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.naming.ConfigurationException;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author quangdt
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Log4j2
@NoArgsConstructor
@Setter
public class CampaignProcess extends TimeBaseProcess{
    @Autowired
    ApplicationContext appContext;
    
    @Autowired
    CampaignActionServices campaignActionService;
    
    volatile Campaign campaign;
    volatile boolean active = false;
    private Map<Long, CampaignActionProcess> mapCampaignActionProcess = new HashMap<>();
    
    /**
     * 
     * 
     * @throws NullPointerException if campaign not set
     */
    public void start() throws NullPointerException{
        if(campaign == null){
            throw new NullPointerException("Campaign info null");
        }
        try {
            init(campaign.getTimeOfDayStart(), campaign.getTimeOfDayStop());
            log.info("Campaign {} run on {}", campaign.getId(), getLstTimer());
        } catch (ConfigurationException ex) {
            log.error("Init time range of campaig {}", campaign.getId(), ex);
        }
        if(isValidTime()){
            active = true;
            //Load running campaign action 
            List<CampaignAction> lstCampaignAction = campaignActionService.findRunningByCampaignID(campaign.getId());
            lstCampaignAction.forEach(campainAction ->{
                startCampaignAction(campainAction);
            });
            //Stop other campaign action
            List<Long> listIDCampaignActRunning = lstCampaignAction.stream().map(
                    CampaignAction::getId).collect(Collectors.toList());
            Set<Long> mapKeys = this.mapCampaignActionProcess.keySet();

            mapKeys.forEach(key -> {
                if (!listIDCampaignActRunning.contains(key)) {
                    stopCampaignAction(key);
                }
            });
        }else{
            stop();
        }
        
    }
    /**
     * Stop all campaign action running
     */
//    @PreDestroy
    public void stop(){
        active = false;
        //TODO: Stop all campaign action running
        Set<Long> keySet = mapCampaignActionProcess.keySet();
        keySet.forEach(key -> {
            stopCampaignAction(key);
        });
        mapCampaignActionProcess.clear();
    }
    
    private void startCampaignAction(CampaignAction campaignAction){
        CampaignActionProcess process = mapCampaignActionProcess.get(campaignAction.getId());
        if(process == null){
            log.info("Starting campaign action process {} ", campaignAction);
            process = appContext.getBean(CampaignActionProcess.class);
            mapCampaignActionProcess.put(campaignAction.getId(), process);
        }else{
            log.info("Update campaign action {}", campaignAction);
        }
        process.setCampaignAction(campaignAction);
        process.setCampaign(campaign);
        process.start();
        
    }
    
    private void stopCampaignAction(Long campaignActionId){
        CampaignActionProcess process = mapCampaignActionProcess.get(campaignActionId);
        if(process != null){
            log.info("Stopping campaign action process {}", campaignActionId);
            process.stop();
            log.info("Stopped campaign action process {}", campaignActionId);
        }
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.threads.init;

import com.its.callbot.callmanage.services.CallConversationService;
import com.its.callbot.callmanage.services.ConfigService;
import com.its.callbot.callmanage.threads.Agent;
import com.its.callbot.commons.entities.CallConversation;
import dtsoft.utils.Util;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.naming.ConfigurationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

/**
 *
 * @author quangdt
 */
//@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
@Log4j2
public class CallInitSchedule extends Agent {

    @Autowired
    ApplicationContext appContext;

    @Autowired
    CallConversationService callConversationService;

    @Autowired
    @Qualifier("callinit.threadpool")
    ExecutorService threadPool;

    @Autowired
    ConfigService configService;

    @PostConstruct
    public void init() throws ConfigurationException {
    }

    /**
     * Schedule to create call
     */
    public void createCall() {
        if (!isClustered()) {
            log.warn("Waiting join to cluster");
            Util.sleep(1000);
            return;
        }
        List<CallConversation> lstCall = callConversationService.findInitCall();
//        System.out.println(lstCall);
        if(lstCall == null){
            Util.sleep(1000);
            return;
        }
        log.info("Conversation init size: {}", lstCall.size());
        if (lstCall.isEmpty()) {
            Util.sleep(1000);
            return;
        }
        List<Future> lstFuture = new ArrayList<>(lstCall.size());
        for (CallConversation call : lstCall) {
            CallInitTask task = appContext.getBean(CallInitTask.class);
            task.setCallConversation(call);
            log.
                    info("Submit create call task for conversation {}", call.
                            getId(), call.getConversationId());
            Future f = threadPool.submit(task);
            lstFuture.add(f);
        }
        if (configService.isInitCallWaitFinish()) {
            lstFuture.forEach((f) -> {
                try {
                    f.get(configService.getInitCallTimeout(),
                            TimeUnit.MILLISECONDS);
                } catch (Exception ex) {
                    log.error("Create call", ex);
                }
            });
        }
    }
}

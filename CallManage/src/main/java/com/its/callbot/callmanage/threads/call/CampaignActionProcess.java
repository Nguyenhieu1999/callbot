/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.threads.call;

import com.dtsoft.clustering.ClusterManager;
import com.its.callbot.callmanage.services.CallConversationService;
import com.its.callbot.callmanage.services.CampaignActionServices;
import com.its.callbot.callmanage.services.ConfigService;
import com.its.callbot.callmanage.thresold.CCUCounter;
import com.its.callbot.commons.config.CallManager;
import com.its.callbot.commons.entities.CallConversation;
import com.its.callbot.commons.entities.Campaign;
import com.its.callbot.commons.entities.CampaignAction;
import dtsoft.utils.Util;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import javax.annotation.PreDestroy;
import javax.naming.ConfigurationException;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author quangdt
 */
@Getter
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Log4j2
@Setter
public class CampaignActionProcess extends TimeBaseProcess {

    @Autowired
    ApplicationContext appContext;

    @Autowired
    private CampaignActionServices campaignActionService;
    @Autowired
    ConfigService configService;
//    @Autowired
//    private CallConversationRepository callConversationRepository;

    @Autowired
    private CallConversationService callConversationService;
    @Autowired
    @Qualifier("callout.threadpool")
    ExecutorService threadPool;
    
    
    boolean active = false;
    CampaignAction campaignAction;
    Campaign campaign;
    private CCUCounter campaignCCUCounter;

    Thread threadRunner = null;

    public synchronized void start() {
        if (campaign == null) {
            throw new NullPointerException("Campaign info null");
        }
        campaignCCUCounter = appContext.getBean(
                CCUCounter.class, campaignAction.getCampaignId());
        try {
            init(campaign.getTimeOfDayStart(), campaign.getTimeOfDayStop());
            log.info("Campaign {} run on {}", campaign.getId(), getLstTimer());

            this.active = true;
            if (threadRunner == null) {
                threadRunner = new Thread(() -> loop());
                threadRunner.start();
            }
        } catch (ConfigurationException ex) {
            log.error("Init time range of campaign {}", campaign.getId(), ex);
        }

    }

    @PreDestroy
    public void stop() {
        this.active = false;
    }

    private void loop() {
//        CalloutTask callTask = appContext.getBean(
//                                    CalloutTask.class);
        while (active) {
            try {
                log.info("Check call conversation campaign action {}",
                        campaignAction.getId());
                
                Date limitDate = new Date(
                        System.currentTimeMillis() - configService.
                        getCalloutDelayMs());
                Date retryDateFrom = new Date(
                        System.currentTimeMillis() - (campaign.getRetryAfter() * 60 * 1000));
                List<CallConversation> callConversations = callConversationService
                        .findCalloutByCampaignActionId(campaignAction.getId(),
                                configService.getRetryCodes(), campaign.
                                getRetryTime(), retryDateFrom,
                                limitDate, configService.getLimitConversation());

                if (callConversations == null || callConversations.isEmpty()) {
                    log.
                            info("No valid call conversation with campaign action id = {}.",
                                    campaignAction.getId());
                    Util.sleep(1000);
                    continue;
                } else {
                    log.info(
                            "Got {} call conversation with campaign action id = {}",
                            callConversations.size(), campaignAction.getId());
                }
                
                List<Future<Integer>> lstFuture = new ArrayList<>();
                try {

                    for (int i = 0; i < callConversations.size();) {
                        //<editor-fold defaultstate="collapsed" desc="Check CCU">
                        if(CallManager.ccuCounter.incrementAndGet() > getCCULimit()){
                            log.info(
                                    "System Limited call current ({}/{}). "
                                            + "Wait to other call finish.",
                                    CallManager.ccuCounter.get() - 1,
                                    getCCULimit());
                            CallManager.ccuCounter.decrementAndGet();
                            Util.sleep(10);
                            continue;
                        }
                        if (campaignCCUCounter.incrementAndGet() > getCampaignCCULimit()) {
                            log.info(
                                    "Campaign {} Limited call current ({}/{}). "
                                            + "Wait to other call finish.",
                                    campaign.getId(),
                                    campaignCCUCounter.get() - 1,
                                    getCampaignCCULimit());
                            campaignCCUCounter.decrementAndGet();
                            CallManager.ccuCounter.decrementAndGet();
                            Util.sleep(10);
                            continue;
                        }
//</editor-fold>

                        if (active && isValidTime()) {
                            CallConversation callConversation = callConversations.
                                    get(i);
                            CalloutTask callTask = appContext.getBean(
                                    CalloutTask.class);
                            callTask.setCallConversation(callConversation);
                            callTask.setCampaignAction(campaignAction);
                            callTask.setCampaignCCUCounter(campaignCCUCounter);

                            Future f = threadPool.submit(callTask);
                            lstFuture.add(f);
                            Util.sleep(configService.getCalloutIntvMs());
                        } else {
                            log.info("Stopping thread or invalid call time");
                            campaignCCUCounter.decrementAndGet();
                            CallManager.ccuCounter.decrementAndGet();
                            break;
                        }
                        i++;
                    }
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                } finally {
                    if(!lstFuture.isEmpty()){
                        int totalCall  = 0;
                        for(Future<Integer> f : lstFuture){
                            totalCall += f.get();
                        }
                        campaignActionService.updateTotalCall(campaignAction.
                            getId(),totalCall);
                    }
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
            Util.sleep(100);
        }
        log.info("Stopped process campaign action {}", campaignAction.getId());
        threadRunner = null;
    }
    
    private int getCCULimit(){
        return configService.getCalloutLimit() / ClusterManager.getMembers();
    }
    
    private int getCampaignCCULimit(){
        return campaign.getConcurrentCall() / ClusterManager.getMembers();
    }
}

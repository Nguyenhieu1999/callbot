/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.threads.call;

import com.its.callbot.bot.CallBotCommunication;
import com.its.callbot.bot.dto.BotDTOFactory;
import com.its.callbot.bot.dto.CallOutRequest;
import com.its.callbot.bot.dto.CallOutResponse;
import com.its.callbot.callmanage.services.BlackListService;
import com.its.callbot.callmanage.services.CallConversationService;
import com.its.callbot.callmanage.services.CallLogService;
import com.its.callbot.callmanage.services.WhitelistService;
import com.its.callbot.callmanage.thresold.CCUCounter;
import com.its.callbot.commons.config.CallManager;
import com.its.callbot.commons.entities.CallConversation;
import com.its.callbot.commons.entities.CallLogEntity;
import com.its.callbot.commons.entities.CampaignAction;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.Callable;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author quangdt
 */
@Data
@Component
@Log4j2
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CalloutTask implements Callable<Integer> {

    @Autowired
    CallBotCommunication callbot;

    @Autowired
    private CallLogService callLogService;
    @Autowired
    private CallConversationService callConversationService;

    @Autowired
    BotDTOFactory botDtoFactory;

    @Autowired
    BlackListService blacklist;
    @Autowired
    WhitelistService whitelist;

    CampaignAction campaignAction;
    CallConversation callConversation;
    private CCUCounter campaignCCUCounter;
    
    

    @Override
    public Integer call() {
        String conversationId = callConversation.getConversationId();

        boolean makeCallSuccess = false;
        CallLogEntity callLogEntity = null;

        try {
            //<editor-fold defaultstate="collapsed" desc="Lock to process">
            if (!callConversationService.getLock(callConversation)) {
                log.info("Can't get lock on conversation {} - {}",
                        callConversation.getId(), conversationId);
                return 0;
            }
            //</editor-fold>
            try {
                log.info("Process conversation {} - {} - {}",
                        callConversation.getId(), conversationId,
                        callConversation.getIsdn());
                String Isdn = callConversation.getIsdn();
                CallLogEntity.Status callLogStatus = null;
                try {
                    //<editor-fold defaultstate="collapsed" desc="Check blacklist">
                    if ((blacklist.contain(Isdn)|| !whitelist.contain(Isdn))) {
                    log.info(
                            "MSISDN {} not exist in whitelist or exist in blacklist",
                            Isdn);
                        callConversation.setStatus(CallConversation.STATUS_BLACKLIST);
                        callLogStatus = CallLogEntity.Status.BLACKLIST;
                        return 0;
                    }
                    //</editor-fold>
                    

                    //<editor-fold defaultstate="collapsed" desc="Call out">
                    CallOutResponse callOutResponseDTO;
                    try {
                        CallOutRequest request = botDtoFactory.getCalloutRequest(conversationId, null);
                        callOutResponseDTO = callbot.callout(request);
                    } catch (IOException ex) {
                        callConversation.setStatus(
                                CallConversation.STATUS_CALL_FAIL);
                        callLogStatus = CallLogEntity.Status.REQUEST_CALLOUT_FAIL;
                        log.error("Request callout fail {} - {} - {}",
                                callConversation.getId(), conversationId,
                                Isdn, ex);
                        return 1;
                    }

                    log.info("[{}] Request CallOut result: {}", Isdn, callOutResponseDTO.getMsg());
                    callLogStatus = CallLogEntity.Status.findByValue(
                            callOutResponseDTO.getStatus());
                    if (Objects.equals(CallLogEntity.Status.CREATED.get(),
                            callOutResponseDTO.getStatus())) {
                        makeCallSuccess = true;
                        callConversation.setStatus(
                                CallConversation.STATUS_CALLED);
                    } else {
                        callConversation.setStatus(
                                CallConversation.STATUS_CALL_FAIL);
                    }
                    //</editor-fold>
                    return 1;
                } finally {

                    //<editor-fold defaultstate="collapsed" desc="Write call log">
                    if (callLogStatus == null) {
                        callLogStatus = CallLogEntity.Status.REQUEST_CALLOUT_FAIL;
                    }
                    callLogEntity = CallLogEntity.generate(campaignAction,
                    		Isdn, conversationId, callLogStatus.get());
                    try {
                        callLogService.save(callLogEntity);
                    } catch (Exception e) {
                        log.error("Save call log error {}", callLogEntity, e);
                    }
                    //</editor-fold>
                }
            } finally {
                try {

                    callConversation.setRetry(callConversation.getRetry() + 1);
                    log.info("Update {}", callConversation);
                    callConversationService.save(callConversation);
                } catch (Exception e) {
                    log.error("Update call conversation fail {}",
                            callConversation, e);
                }
            }
        } finally {
            if (!makeCallSuccess) {
                //Neu make call khong thanh cong => Giam CCU
                campaignCCUCounter.decrementAndGet();
                CallManager.ccuCounter.decrementAndGet();
            } else {
                //Make call thành công, monitor call
//                monitorningCall.add(conversationId, callLogEntity);
            }
        }
    }

    
}

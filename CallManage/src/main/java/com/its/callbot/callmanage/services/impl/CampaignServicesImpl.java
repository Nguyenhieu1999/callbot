/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.services.impl;

import com.its.callbot.callmanage.services.CampaignServices;
import com.its.callbot.commons.entities.Campaign;
import com.its.callbot.commons.repositories.CampaignRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CampaignServicesImpl implements CampaignServices {

    @Autowired
    CampaignRepository repo;
    
    @Override
    public List<Campaign> getByStatus(int status) {
        Calendar cal = Calendar.getInstance();
        int date = cal.get(Calendar.DATE);
        List<Campaign> lstCampaign = repo.findByStatus(status);
        List<Campaign> lstValidCampaign = new ArrayList<>();
        lstCampaign.stream().
                filter((campaign) -> (campaign.validDate(date))).
                forEachOrdered((campaign) -> {
                    lstValidCampaign.add(campaign);
                }
        );
        return lstValidCampaign;
    }
    
}

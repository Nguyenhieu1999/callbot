/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.services.impl;

import com.dtsoft.clustering.ClusterManager;
import com.its.callbot.callmanage.repositories.CallRepository;
import com.its.callbot.callmanage.services.CallConversationService;
import com.its.callbot.callmanage.services.ConfigService;
import com.its.callbot.commons.entities.CallConversation;
import java.util.Date;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallConversationServiceImpl implements CallConversationService {

    @Autowired
    CallRepository repo;

    @Autowired
    ConfigService configService;

    @Override
    public List<CallConversation> findInitCall() {
    	 Pageable pageable = PageRequest.of(0, configService.getInitCallLimit(),
                 Sort.by(Sort.Direction.ASC, "createdAt"));
        Page<CallConversation> p = repo.findInitCall(configService.
                getInitCallMaxRetry(),
                ClusterManager.getMembers(), ClusterManager.getOrder(),
                pageable);
        if (p == null) {
            return null;
        }
        return p.getContent();
        
    }

    @Override
    public void save(CallConversation callConversation) {
        repo.save(callConversation);
    }

    @Override
    public List<CallConversation> findCalloutByCampaignActionId(
            Long campaignActionId,
            List<Integer> retryCodes, Integer retryTime, Date retryDateFrom,
            Date limitDate, int limitConversation) {
        Pageable pageable = PageRequest.of(0, limitConversation,
                Sort.by(Sort.Direction.ASC, "createdAt"));

        Page<CallConversation> page = repo.findByCampaignActionId(
                campaignActionId,
                retryCodes, retryTime, retryDateFrom, limitDate,
                ClusterManager.getMembers(), ClusterManager.getOrder(),
                pageable);
        if (page == null) {
            return null;
        }
        return page.getContent();
    }
    @Override
    public boolean getLock(CallConversation callConversation) {
        return repo.getLock(callConversation);
    }
    

}

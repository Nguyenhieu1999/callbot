/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.repositories;

import org.springframework.stereotype.Repository;

/**
 *
 * @author quangdt
 */@Repository
public interface CampaignActionExtRepository 
         extends com.its.callbot.commons.repositories.CampaignActionRepository, CampaignActionExtRepositoryCustom{
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.repositories;

import com.its.callbot.commons.entities.CallConversation;
import java.util.List;

/**
 *
 * @author quangdt
 */
public interface CallRepositoryCustom {
//    public void updateStatus(String conversationId, int newStatus);
        
    public boolean getLock(CallConversation conversation);
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.repositories.impl;

import com.its.callbot.callmanage.repositories.CampaignActionExtRepositoryCustom;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Repository
@Transactional
public class CampaignActionExtRepositoryCustomImpl implements CampaignActionExtRepositoryCustom {
    @Autowired
    EntityManager entityManager;
    
    @Override
    public void updateTotalCall(long campaignActionId, long numberCall) {
        log.info("try update campaign action {} add more number call {}",
                campaignActionId, numberCall);
        try {
            Query query = entityManager.
                    createNativeQuery(SQL_UPDATE_NUMBER_CALL);
            query.setParameter("numCall", numberCall);
            query.setParameter("campaignActionId", campaignActionId);

            int rowAffected = query.executeUpdate();
            if (rowAffected > 0) {
                log.info("Updated campaign action {} add more number call {}",
                        campaignActionId, numberCall);
            } else {
                log.warn(
                        "Update fail campaign action {} add more number call {}",
                        campaignActionId, numberCall);
            }
        } catch (Exception e) {
            log.error("Update error campaign action {} add more number call {}",
                    campaignActionId, numberCall, e);
        }
    }
    String SQL_UPDATE_NUMBER_CALL = "update campaign_action set total_call = ifnull(total_call, 0)  + :numCall "
            + " where id = :campaignActionId";
    
}

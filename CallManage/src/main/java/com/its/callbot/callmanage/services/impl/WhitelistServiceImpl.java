/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.services.impl;

import com.its.callbot.callmanage.services.WhitelistService;
import com.its.callbot.commons.entities.Whitelist;
import com.its.callbot.commons.repositories.WhitelistRepository;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class WhitelistServiceImpl extends SpecialCustomerService implements WhitelistService {

    @Autowired
    WhitelistRepository repo;
    
    @Scheduled(cron = "0 * * * * *")
    public void reload(){
        List<Whitelist> lstWhitelist = repo.findAll();
        List<String> whitelist = new ArrayList<>(lstWhitelist.size());
        lstWhitelist.forEach((item) ->{
            whitelist.add(item.getIsdn());
        });
        List temp = listSpecialCustomer ;
        listSpecialCustomer = whitelist;
        log.info("Blacklist size {}", listSpecialCustomer.size());
        temp.clear();
        temp = null;
        
    }
}

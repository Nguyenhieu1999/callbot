/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.services.impl;

import com.its.callbot.callmanage.services.CallLogService;
import com.its.callbot.commons.entities.CallLogEntity;
import com.its.callbot.commons.repositories.CallLogRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Log4j2
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallLogServiceImpl implements CallLogService {

    @Autowired
    CallLogRepository repo;
    
    @Override
    public void save(CallLogEntity callLogEntity) {
        repo.save(callLogEntity);
    }
    
}

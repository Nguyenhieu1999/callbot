/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.config;

import com.dtsoft.clustering.ClusterManager;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Scope;

/**
 *
 * @author quangdt
 */
@Log4j2
@Getter
@Configuration
@ImportResource({"${app.context}"})
public class AppConfig {
    @Bean
    CommandLineRunner initAgent(@Value("${agent.cfg}") String agentConfig) {
         return args -> {
             ClusterManager.init(agentConfig);
         };
    }
    @Bean
    @Qualifier("callinit.threadpool")
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public ExecutorService getCallInitThreadPool(@Value("${threadpool.initcall.size}") int poolSize){
        return Executors.newFixedThreadPool(poolSize);
    }
    
    @Bean
    @Qualifier("callout.threadpool")
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public ExecutorService getCalloutThreadPool(@Value("${threadpool.callout.size}") int poolSize){
        return Executors.newFixedThreadPool(poolSize);
    }
}

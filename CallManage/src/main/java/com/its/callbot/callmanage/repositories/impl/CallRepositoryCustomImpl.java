/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.repositories.impl;

import com.its.callbot.callmanage.repositories.CallRepositoryCustom;
import com.its.callbot.commons.entities.CallConversation;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
@Transactional
@Log4j2
public class CallRepositoryCustomImpl implements CallRepositoryCustom {

    @Autowired
    EntityManager entityManager;
    

    @Override
    public boolean getLock(CallConversation conversation) {
        long id = conversation.getId();
        int currentStatus = conversation.getStatus();
        log.info("Try lock call conversation {}", id);
        try {
            Query query = entityManager.createNativeQuery(SQL_LOCK);
            query.setParameter("id", id);
            query.setParameter("status", currentStatus);

            int rowAffected = query.executeUpdate();
            if(rowAffected > 0){
                log.info("Locked call conversation {}", id);
                return true;
            }else{
                log.info("Lock fail call conversation {}", id);
                return false;
            }
        } catch (Exception e) {
            log.error("Lock error call conversation {}", id, e);
            return false;
        }
    }
    
    
    static final String SQL_LOCK = "update call_queue set status = "+CallConversation.STATUS_CALLING+" where id = :id and status = :status";

	

}

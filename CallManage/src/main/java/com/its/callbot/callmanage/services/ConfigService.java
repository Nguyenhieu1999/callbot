/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.services;

import java.util.List;

/**
 *
 * @author quangdt
 */
public interface ConfigService {

    public boolean isInitCallWaitFinish();

    public long getInitCallTimeout();
    
    public int getInitCallLimit();
    
    public int getInitCallMaxRetry();

    public String getNationalCode();

    public List<Integer> getBlackCalendarDay();

    public List<Integer> getBlackLunarDay();

    public long getCalloutDelayMs();

    public long getCalloutIntvMs();

    public int getCalloutLimit();
    
    public List<Integer> getRetryCodes();
    
    public int getLimitConversation();
}

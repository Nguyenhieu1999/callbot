/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.threads;

import com.dtsoft.clustering.ClusterManager;

/**
 *
 * @author quangdt
 */
public abstract class Agent {
    
    protected boolean isClustered(){
        return ClusterManager.getMembers() > 0;
    }
    
    protected int getMembers(){
        return ClusterManager.getMembers();
    }
    
    protected int getOrder(){
        return ClusterManager.getOrder();
    }
}

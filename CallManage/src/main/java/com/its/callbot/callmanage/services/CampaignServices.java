/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.services;

import com.its.callbot.commons.entities.Campaign;
import java.util.List;

/**
 *
 * @author quangdt
 */
public interface CampaignServices {

    public List<Campaign> getByStatus(int value);
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.services.impl;

import com.its.callbot.callmanage.repositories.CampaignActionExtRepository;
import com.its.callbot.callmanage.services.CampaignActionServices;
import com.its.callbot.commons.entities.CampaignAction;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Log4j2
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CampaignActionServicesImpl implements CampaignActionServices {

    @Autowired
    CampaignActionExtRepository repo;
    
    @Override
    public List<CampaignAction> findRunningByCampaignID(Integer campaignId) {
        return repo.findRunningByCampaignId(campaignId);
    }

    @Override
    public void updateTotalCall(Long campaignActionId, int totalCall) {
        repo.updateTotalCall(campaignActionId, totalCall);
    }
    
}

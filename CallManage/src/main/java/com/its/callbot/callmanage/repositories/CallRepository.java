/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.repositories;

import com.its.callbot.commons.entities.CallConversation;
import com.its.callbot.commons.repositories.BaseRepository;

import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author quangdt
 */
@Repository
public interface CallRepository extends
 BaseRepository<CallConversation, Long>,
        CallRepositoryCustom {

    @Query(value = "SELECT cc FROM CallConversation cc"
            + " WHERE cc.campaignAction.id = :campaignActionId"
//            + " AND cc.contactId.id = :contactId"
            + " AND created_at <= :createCallAfter")
    CallConversation findByCampaignActionId(@Param(
            "campaignActionId") Long campaignActionId,
//            @Param("contactId") Long contactId,
            @Param("createCallAfter") Date createCallAfter);

    @Query(value = "SELECT cc FROM CallConversation cc"
            + " WHERE cc.campaignAction.id = :campaignActionId"
            + " AND ("
            + "         (cc.status = 0 AND cc.updatedAt <= :createCallAfter)"
            + "      OR (cc.status in :lstStatus AND cc.retry < :retry AND cc.updatedAt <= :lastCallAt)"
            + " )"
            + " and MOD(cc.id, :mode) = :remainder")
    Page<CallConversation> findByCampaignActionId(
            @Param("campaignActionId") Long campaignActionId,
            @Param("lstStatus") List<Integer> status,
            @Param("retry") int maxRetry,
            @Param("lastCallAt") Date lastCallAt,
            @Param("createCallAfter") Date createCallAfter,
            @Param("mode") int mode, @Param("remainder") int remainder,
            Pageable pageable);
    
    @Query(value = "SELECT cc FROM CallConversation cc"
            + " WHERE  (cc.status = -1 and cc.retry < :retry) "
            + " and MOD(cc.id, :mode) = :remainder")
    Page<CallConversation> findInitCall(@Param("retry") int retry, @Param("mode") int mode, @Param("remainder") int remainder,
    		Pageable pageable);
}

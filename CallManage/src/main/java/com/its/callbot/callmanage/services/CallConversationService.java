/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.services;

import com.its.callbot.commons.entities.CallConversation;
import java.util.Date;
import java.util.List;

/**
 *
 * @author quangdt
 */
public interface CallConversationService {
    
    public List<CallConversation> findInitCall();

    public void save(CallConversation callConversation);

    public List<CallConversation> findCalloutByCampaignActionId(Long id,
            List<Integer> retryCodes, Integer retryTime, Date retryDateFrom,
            Date limitDate, int limitConversation);
    public boolean getLock(CallConversation callConversation);
}

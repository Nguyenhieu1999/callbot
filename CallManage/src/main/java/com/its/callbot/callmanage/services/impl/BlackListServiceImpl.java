/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.services.impl;

import com.its.callbot.callmanage.services.BlackListService;
import com.its.callbot.commons.entities.Blacklist;
import com.its.callbot.commons.repositories.BlackListRepository;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class BlackListServiceImpl extends SpecialCustomerService implements BlackListService {
    
    @Autowired
    BlackListRepository repo;
    
    @Scheduled(cron = "0 * * * * *")
    public void reload(){
        List<Blacklist> lstBlacklist = repo.findAll();
        List<String> blacklist = new ArrayList<>(lstBlacklist.size());
        lstBlacklist.forEach((item) ->{
            blacklist.add(item.getIsdn());
        });
        List temp = listSpecialCustomer ;
        listSpecialCustomer = blacklist;
        log.info("Blacklist size {}", listSpecialCustomer.size());
        temp.clear();
        temp = null;
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.services.impl;

import com.its.callbot.callmanage.services.ConfigService;
import com.its.callbot.commons.entities.Config;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.naming.ConfigurationException;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Data
@EqualsAndHashCode(callSuper = true)
@Log4j2
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class ConfigServiceImpl extends com.its.callbot.commons.services.ConfigService
        implements ConfigService {

    public boolean initCallWaitFinish;

    public long initCallTimeout;

    public int initCallLimit;
    
    public int initCallMaxRetry;

    public List<Integer> BlackCalendarDay;

    public List<Integer> BlackLunarDay;

    public long CalloutDelayMs;

    public long CalloutIntvMs;

    public int CalloutLimit;

    public List<Integer> RetryCodes;

    public int LimitConversation;
    
//    @PostConstruct
    @Scheduled(fixedRate = 60000)
    public void reload() throws ConfigurationException{
        CalloutLimit = findByKey("call.limit").getIntValue();
        CalloutDelayMs = findByKey("call.delay.after.init").getIntValue();
        CalloutIntvMs = findByKey("call.intv").getIntValue();
        LimitConversation = findByKey("call.queue.limit.query").getIntValue();
        RetryCodes = findByKey("call.retry.codes").getListIntValue();
        initCallLimit = findByKey("call.init.limit.query").getIntValue();
        initCallWaitFinish = findByKey("call.init.wait").getBooleanValue();
        initCallTimeout = findByKey("call.init.timeout").getIntValue();
        initCallMaxRetry = findByKey("call.init.retry.max").getIntValue();
        BlackCalendarDay = findByKey("blackday.calendar").getListIntValue();
        BlackLunarDay = findByKey("blackday.calendar.lunar").getListIntValue();
        log.info(this);
    }
    
    public Config findByKey(String key) {
        return repo.findByKey(key);
    }

}

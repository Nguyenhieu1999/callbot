/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.threads.call;

import com.ibm.icu.util.ChineseCalendar;
import com.its.callbot.callmanage.services.CampaignServices;
import com.its.callbot.callmanage.services.ConfigService;
import com.its.callbot.callmanage.threads.Agent;
import com.its.callbot.commons.entities.Campaign;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author quangdt
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
@Log4j2
public class CampaignManager extends Agent{
    @Autowired
    ApplicationContext appContext;

    @Autowired
    CampaignServices campaignService;

    @Autowired
    ConfigService configService;

    private boolean active = true;
//    private List<Integer> lstStatus;
    private final Map<Integer, CampaignProcess> mapCampaignProcess = new HashMap<>();

    @PostConstruct
    public void init() throws Exception {
    
    }

    @PreDestroy
    public void stop() {
        this.active = true;
        
        Set<Integer> lstCampaignProcess = mapCampaignProcess.keySet();
        stopCampaign(lstCampaignProcess);

    }

    public void process() {
        if (!active) {
            return;
        }
        if(!isClustered()){
            log.info("Waiting join to cluster");
            return;
        }
        
        if(isBlackDays()){
            Set<Integer> mapKeys = this.mapCampaignProcess.keySet();
            stopCampaign(mapKeys);
            return;
        }
        
        List<Campaign> lstActiveCampaign = campaignService.
                getByStatus(Campaign.STATUS.RUNNING.getValue());
        log.info("Number campaign running today is {}", lstActiveCampaign.size());
        //Update Active campaign
        lstActiveCampaign.forEach((campaign) -> {
            this.startCampaign(campaign);
        });
        //Stop cac campaign dang chay khac
        List<Integer> listIDCampaignRunning = lstActiveCampaign.stream().map(
                Campaign::getId).collect(Collectors.toList());
        Set<Integer> mapKeys = this.mapCampaignProcess.keySet();
        List<Integer> stopingCampaign = new ArrayList<>();
        mapKeys.forEach(key -> {
            if (!listIDCampaignRunning.contains(key)) {
                stopingCampaign.add(key);
            }
        });
        stopCampaign(stopingCampaign);
    }

    private void startCampaign(Campaign campaign) {
        CampaignProcess process = mapCampaignProcess.get(campaign.getId());
        if(process == null){
            process = appContext.getBean(CampaignProcess.class);
            mapCampaignProcess.put(campaign.getId(), process);
            log.info("Starting campaign process {}", campaign);
        }else{
            log.info("Update campaign info {}", campaign);
        }
        process.setCampaign(campaign);
        try{
            process.start();
        }catch(NullPointerException e){
            log.info("Start campaign process {} fail. Campaign null", campaign);
        }
    }

    private void stopCampaign(Collection<Integer> campaignIds) {
        
        for(Integer campaignId :campaignIds){
            log.info("Terminate campaign id {}", campaignId);
            CampaignProcess campaignProcess = this.mapCampaignProcess.get(campaignId);
            if (campaignProcess != null) {
                campaignProcess.stop();
            }
            log.info("Terminate campaign id {} successfully.", campaignId);
        }
        campaignIds.forEach(campaignId -> {
            this.mapCampaignProcess.remove(campaignId);
        });
        
    }

    private boolean isBlackDays() {
        Calendar cal = Calendar.getInstance();
        int date = cal.get(Calendar.DATE);
        log.debug("blackDays: {}", configService.getBlackCalendarDay());
        if(configService.getBlackCalendarDay().contains(date)){
            log.info("Today({}/{}) is in blackDays. No call", date, cal.get(Calendar.MONTH));
            return true;
        }
        ChineseCalendar chineseCalendar = new ChineseCalendar(cal.getTime());
        int chineseDate = chineseCalendar.get(ChineseCalendar.DATE);
        log.debug("getBlackLunarDay: {}", configService.getBlackLunarDay());
        if(configService.getBlackLunarDay().contains(chineseDate)){
            log.info("Today({}/{}) is in blackLunarDays. No call", 
                    chineseDate, chineseCalendar.get(ChineseCalendar.MONTH));
            return true;
        }
        return false;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.services.impl;

import com.its.callbot.callmanage.services.ConfigService;
import dtsoft.utils.MSISDN;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author quangdt
 */
public class SpecialCustomerService {
    @Autowired
    ConfigService config;
    
    protected List<String> listSpecialCustomer = new ArrayList<>();
    
    public boolean contain(String msisdn) {
        String isdn = MSISDN.toIsdn(msisdn, config.getNationalCode());
        return listSpecialCustomer.contains(isdn);
    }
}

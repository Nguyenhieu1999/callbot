/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.threads.call;

import com.its.callbot.callmanage.utils.TimerValidator;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.naming.ConfigurationException;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author quangdt
 */
@Log4j2
@Getter
public abstract class TimeBaseProcess {

    final List<TimerValidator> lstTimer = Collections.synchronizedList(new ArrayList<>());

    protected boolean isValidTime() {
        
        LocalTime now = LocalTime.now();

        int currHour = now.getHour();
        int currMinute = now.getMinute();
//        log.info("Valid time is {}", lstTimer);
//        log.info("Currrent time is {}", now);
        return lstTimer.stream().
                anyMatch((timer) -> (timer.accept(currHour, currMinute)));
    }

    protected void init(String timeStart, String timeEnd) throws
            ConfigurationException {
        String[] tempStart = timeStart.trim().split(";");

        String[] tempEnd = timeEnd.trim().split(";");

        if (tempEnd.length != tempStart.length) {
            throw new ConfigurationException(
                    "Time ranges (From "+timeStart+" to "+timeEnd+") not matches size.");
        }
        lstTimer.clear();
        for (int i = 0; i < tempStart.length; i++) {
            TimerValidator timer = new TimerValidator(tempStart[i],
                    tempEnd[i]);
            lstTimer.add(timer);
        }
    }
}

package com.its.callbot.callmanage.threads.init;

import com.its.callbot.bot.CallBotCommunication;
import com.its.callbot.bot.dto.BotDTOFactory;
import com.its.callbot.bot.dto.CreateCallRequest;
import com.its.callbot.bot.dto.CreateCallResponse;
import com.its.callbot.callmanage.services.CallConversationService;
import com.its.callbot.callmanage.services.ConfigService;
import com.its.callbot.commons.entities.CallConversation;
import dtsoft.utils.MSISDN;
import java.io.IOException;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author quangdt
 */
@Setter
@Component
@Log4j2
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallInitTask implements Runnable {

    @Autowired
    CallBotCommunication callbot;
    @Autowired
    CallConversationService callConversationService;

    @Autowired
    BotDTOFactory botDtoFactory;
    
    @Autowired
    ConfigService config;

    CallConversation callConversation;

    @Override
    public void run() {
        log.info("Create call for {}", callConversation.getId());
        String phoneNo = "0" + MSISDN.toIsdn(callConversation.getIsdn(),
                        config.getNationalCode());
        ////////////////////////////////////////////////////////////////////

        CreateCallRequest createCallRequest = botDtoFactory.getInitRequest(
                callConversation.getHotline(), phoneNo, callConversation.getBotId(),
                callConversation.getBotRegion(), callConversation.getInputslots());
        CreateCallResponse createCallResponse = null;
        try {
            createCallResponse = callbot.createCall(createCallRequest);
        } catch (IOException ex) {
            log.error("Create call {} fail", callConversation.getId(), ex);
        }
//            Save call queue
        if (createCallResponse == null) {
            log.warn("Create call false. Server return null");
            return;
        }

        if (createCallResponse.isSuccess()) {
            log.
                    warn("Create call success for {} - {}", callConversation.
                            getId(), createCallResponse.getConversationId());
            callConversation.setConversationId(createCallResponse.
                    getConversationId());
            callConversation.setStatus(CallConversation.STATUS_NEW);

        } else {
            callConversation.setRetry(callConversation.getRetry() + 1);
            log.warn("Create call false {}", createCallRequest);
        }
        log.info("Update {}", callConversation);
        callConversationService.save(callConversation);
    }
}

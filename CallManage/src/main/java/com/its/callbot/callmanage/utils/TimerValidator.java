/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.its.callbot.callmanage.utils;

import dtsoft.utils.StringUtil;

/**
 *
 * @author quangdt
 */

public class TimerValidator {

    private final int hourStart;
    private final int minuteStart;
    private final int hourEnd;
    private final int minuteEnd;

    public TimerValidator(String timeStartString, String timeEndString) {
        String[] temp = timeStartString.split(":");
        this.hourStart = Integer.parseInt(temp[0]);
        this.minuteStart = Integer.parseInt(temp[1]);

        temp = timeEndString.split(":");
        this.hourEnd = Integer.parseInt(temp[0]);
        this.minuteEnd = Integer.parseInt(temp[1]);
    }

    public boolean accept(int currHour, int currMinute) {
        if (currHour < this.hourStart) {
            return false;
        }
        if (currHour == this.hourStart && currMinute < this.minuteStart) {
            return false;
        }
        if (currHour > this.hourEnd) {
            return false;
        }
        if (currHour == this.hourEnd && currMinute >= this.minuteEnd) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TimerRange{From "
                + StringUtil.fill(hourStart, '0', 2, false)+":"+StringUtil.fill(minuteStart, '0', 2, false) 
                +" To "
                +StringUtil.fill(hourEnd, '0', 2, false)+":"+StringUtil.fill(minuteEnd, '0', 2, false)+"}";
    }
    
}

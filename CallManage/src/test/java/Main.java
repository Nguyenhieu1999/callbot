import java.lang.reflect.Field;

public class Main {
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		Student s = new Student();
		s.setName("Quang");
		
		String fieldName = "name";
		String value;
		
		System.out.print(s.getClass().getField(fieldName).get(s));
	}
}
